# Welcome to Nyatakan!

### Stack yang nyatakan pakai :

| Stack     | language        |
| --------- | --------------- |
| Front-End | React.JS (Next) |
| Back-End  | Node            |

### Stack yang nyatakan pakai :

| Service             | Port | Directory  |
| ------------------- | ---- | ---------- |
| Client              | 3000 | ./client   |
| GraphQL API Gateway | 4000 | ./server   |
| Api Node.JS         | 5000 | ./services |
