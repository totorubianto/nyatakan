import { Component } from 'react';
import Router from 'next/router';
import nextCookie from 'next-cookies';
import checkLoggedIn from './checkLoggedIn';
import redirect from './redirect';

// Gets the display name of a JSX component for dev tools
const getDisplayName = Component => Component.displayName || Component.name || 'Component';

function withAuthSync(WrappedComponent) {
  return class extends Component {
    static displayName = `withAuthSync(${getDisplayName(WrappedComponent)})`;

    static async getInitialProps(ctx) {
      const { loggedInUser } = await checkLoggedIn(ctx.apolloClient);
      const token = auth(ctx);

      const componentProps =
        WrappedComponent.getInitialProps && (await WrappedComponent.getInitialProps(ctx, loggedInUser));

      return { ...componentProps, token };
    }

    render() {
      return <WrappedComponent {...this.props} />;
    }
  };
}

function auth(ctx) {
  const { token } = nextCookie(ctx);

  if (ctx.pathname === '/login') {
    /*
     * If `ctx.req` is available it means we are on the server.
     * Additionally if there's no token it means the user is not logged in.
     */
    if (ctx.req && token) {
      redirect(ctx, '/');
    }
    // We already checked for server. This should only happen on client.
    if (token) {
      Router.push('/');
    }
  } else {
    /*
     * If `ctx.req` is available it means we are on the server.
     * Additionally if there's no token it means the user is not logged in.
     */
    if (ctx.req && !token) {
      redirect(ctx, '/login');
    }
    // We already checked for server. This should only happen on client.
    if (!token) {
      Router.push('/login');
    }
  }

  return token;
}

export { withAuthSync, auth };
