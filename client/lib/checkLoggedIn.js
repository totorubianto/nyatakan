import gql from 'graphql-tag';

const CURRENT_USER_QUERY = gql`
  query {
    me {
      username
      email
      userstatus
      profile {
        fullname
        dateofbirth
        job
        phone
        address
        city
        region
        state
        userclass
        picture
      }
      errors {
        value
        param
      }
    }
  }
`;

export default apolloClient =>
  apolloClient
    .query({
      query: CURRENT_USER_QUERY
    })
    .then(data => ({ loggedInUser: data }))
    .catch(err => ({ loggedInUser: {} }));

export { CURRENT_USER_QUERY };
