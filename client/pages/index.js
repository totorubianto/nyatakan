import React from 'react';
import { withApollo } from '../lib/apollo';
import Page from '../components/Page';
import HomeWrapper from '../components/home/HomeWrapper';
import checkLoggedIn from '../lib/checkLoggedIn';

const home = ({ loggedInUser: { data } }) => {
  return (
    <Page>
      <HomeWrapper data={data} />
    </Page>
  );
};

home.getInitialProps = async context => {
  const { loggedInUser } = await checkLoggedIn(context.apolloClient);
  
  if(Object.keys(loggedInUser).length > 0) {
    if(loggedInUser.data.me.errors !== null) {
      return { loggedInUser: {} };
    }
  }
  return { loggedInUser };
};

export default withApollo(home, {
  // Disable apollo ssr fetching in favour of automatic static optimization
  ssr: false
});
