import React from 'react';
import SignupLoginLayout from '../components/layout/signuplogin/SignupLoginLayout';
import ForgotPassWrapper from '../components/forgotpassword/ForgotPassWrapper';
import Page from '../components/Page';
import checkLoggedIn from '../lib/checkLoggedIn';
import {withApollo} from '../lib/apollo';

const forgotPassword = ({ loggedInUser }) => {
  const { data } = loggedInUser;

  return (
    <Page>
      <SignupLoginLayout data={data}>
        <ForgotPassWrapper />
      </SignupLoginLayout>
    </Page>
  );
};

forgotPassword.getInitialProps = async context => {
  const { loggedInUser } = await checkLoggedIn(context.apolloClient);

  if (Object.keys(loggedInUser).length > 0) {
    if(loggedInUser.data.me.errors !== null) {
      return { loggedInUser: {} };
    }
  }

  return { loggedInUser };
};

export default withApollo(forgotPassword);
