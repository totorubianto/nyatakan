import React from 'react';
import SignupLoginLayout from '../components/layout/signuplogin/SignupLoginLayout';
import { withApollo } from '../lib/apollo';
import { withAuthSync } from '../lib/auth';
import Page from '../components/Page';
import LoginWrapper from '../components/login/LoginWrapper';
import checkLoggedIn from '../lib/checkLoggedIn';

const login = ({ loggedInUser: { data } }) => {
  return (
    <Page>
      <SignupLoginLayout data={data}>
        <LoginWrapper />
      </SignupLoginLayout>
    </Page>
  );
};

login.getInitialProps = async context => {
  const { loggedInUser } = await checkLoggedIn(context.apolloClient);

  if (loggedInUser.data.me.errors !== null) {
    return { loggedInUser: {} };
  }

  return { loggedInUser };
};

export default withApollo(withAuthSync(login));
