import React from 'react';
import SignupLoginLayout from '../components/layout/signuplogin/SignupLoginLayout';
import SignupWrapper from '../components/signup/SignupWrapper';
import Page from '../components/Page';
import { withApollo } from '../lib/apollo';
import redirect from '../lib/redirect';
import checkLoggedIn from '../lib/checkLoggedIn';

const signup = ({ loggedInUser: { data }, step }) => {
  return (
    <Page>
      <SignupLoginLayout data={data}>
        <SignupWrapper step={step} />
      </SignupLoginLayout>
    </Page>
  );
};

signup.getInitialProps = async context => {
  const { loggedInUser } = await checkLoggedIn(context.apolloClient);

  if (Object.keys(loggedInUser).length > 0) {
    if (!loggedInUser.data.me.username) {
      return { loggedInUser: {} };
    }

    if (!loggedInUser.data.me.profile) {
      return { step: 1, loggedInUser };
    } else {
      redirect(context, '/');
    }

    if (!loggedInUser.data.me.errors) {
      return { loggedInUser: {} };
    }
  }

  return { loggedInUser };
};

export default withApollo(signup);
