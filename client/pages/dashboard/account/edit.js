import React from 'react';
import DashboardLayout from '../../../components/layout/dashboard/DashboardLayout';
import EditProfile from '../../../components/admin/account/EditProfile';
import redirect from '../../../lib/redirect';
import { withAuthSync } from '../../../lib/auth';
import { withApollo } from '../../../lib/apollo';
import Page from '../../../components/Page';

const edit = ({ loggedInUser: { data } }) => {
  return (
    <Page>
      <DashboardLayout data={data}>
        <EditProfile />
      </DashboardLayout>
    </Page>
  );
};

edit.getInitialProps = async (ctx, loggedInUser) => {
  if (Object.keys(loggedInUser).length > 0) {
    if (!loggedInUser.data.me.profile) {
      redirect(ctx, '/signup');
    }
  }

  return { loggedInUser };
};

export default withApollo(withAuthSync(edit));
