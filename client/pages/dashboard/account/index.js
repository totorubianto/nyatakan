import React from 'react';
import DashboardLayout from '../../../components/layout/dashboard/DashboardLayout';
import AdminAccount from '../../../components/admin/account/AdminAccount';
import redirect from '../../../lib/redirect';
import { withAuthSync } from '../../../lib/auth';
import { withApollo } from '../../../lib/apollo';
import Page from '../../../components/Page';

const adminAccount = ({ loggedInUser: { data } }) => {
  return (
    <Page>
      <DashboardLayout data={data}>
        <AdminAccount />
      </DashboardLayout>
    </Page>
  );
};

adminAccount.getInitialProps = async (ctx, loggedInUser) => {
  if (Object.keys(loggedInUser).length > 0) {
    if (!loggedInUser.data.me.profile) {
      redirect(ctx, '/signup');
    }
  }

  return { loggedInUser };
};

export default withApollo(withAuthSync(adminAccount));
