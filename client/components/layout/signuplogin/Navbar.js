import React from 'react';
import Link from 'next/link';
import styled from 'styled-components';
import { Button } from '../../styles/Button';

const Navbar = styled.nav`
  padding: 24px 120px 16px;

  .navbar {
    &-logo {
      width: 130px;
    }

    &-icon {
      &-down {
        width: 22px;

        :hover {
          cursor: pointer;
        }
      }
    }

    &-search {
      width: 290px;
      height: 36px;
      padding-left: 35px;
      border-radius: 6px;
      border: solid 1px #e2e2e2;
      background-color: #ffffff;

      &::placeholder {
        opacity: 0.6;
        font-family: 'Open Sans', sans-serif;
        border-radius: 6px;
        color: #092041;
      }

      &--icon {
        width: 12px;
        top: 12px;
        left: 30px;
      }
    }
  }

  .btn-login {
    width: 131px;
    cursor: pointer;
    font-family: Open Sans, sans-sherif;
    text-align: center;
    font-size: 16px;
    line-height: 1.25;
    border-radius: 6px;
    border: solid 1px #ffffff;
    padding: 9px 16px;
    color: #fff;
    background-color: transparent;
  }

  @media (max-width: 1024px) {
    .navbar-search {
      width: 225px;
    }
  }

  @media (max-width: 768px) {
    padding: 14px 50px;
    .navbar-search {
      width: 290px;
    }
  }

  @media (max-width: 574px) {
    padding: 14px 20px;
  }
`;

const Nav = ({ data }) => {
  return (
    <Navbar className='navbar navbar-expand-lg navbar-dark'>
      <div className='row align-items-center'>
        <div className='col'>
          <img
            src='/static/logo/nyatakan_white.svg'
            className='navbar-logo'
            alt='Nyatakan Logo'
          />
        </div>
        <div className='d-none d-md-block col position-relative'>
          <input
            className='navbar-search form-control form-control-sm'
            type='search'
            placeholder='Cari merchandise, store, user disini'
            aria-label='Search'
          />
          <img
            src='/static/icons/icon_search.svg'
            className='position-absolute navbar-search--icon'
            alt=''
          />
        </div>
      </div>

      <button
        className='navbar-toggler'
        type='button'
        data-toggle='collapse'
        data-target='#navbarSupportedContent'
        aria-controls='navbarSupportedContent'
        aria-expanded='false'
        aria-label='Toggle navigation'
      >
        <span className='navbar-toggler-icon'></span>
      </button>
      <div className='collapse navbar-collapse' id='navbarSupportedContent'>
        <ul className='navbar-nav ml-auto'>
          <li className='nav-item' style={{ paddingRight: '24px' }}>
            <Button primary lg className='btn'>
              Mulai Desain
            </Button>
          </li>
          <li className='nav-item' style={{ paddingRight: '24px' }}>
            <Link href='/login'>
              <Button transparent lg className='btn'>
                Login
              </Button>
            </Link>
          </li>
          {/* <li className="nav-item">
            <Text md color="white">
              toto.rubiaonto@gmail.com
            </Text>
            <img src="/static/icons/icon_down.svg" className="pl-2 navbar-icon-down" alt="Icon Down" />
          </li> */}
        </ul>
      </div>
    </Navbar>
  );
};

export default Nav;
