import React from 'react';
import Nav from '../Navbar';
import Footer from '../Footer';

function SignupLoginLayout(props) {
  return (
    <div style={{ backgroundImage: 'linear-gradient(199deg, #154376, #092041)' }}>
      <Nav data={props.data} />
      {props.children}
      <Footer />
    </div>
  );
}

export default SignupLoginLayout;
