import React from 'react';
import Router from 'next/router';
import Link from 'next/link';
import gql from 'graphql-tag';
import useToggle from '../hooks/useToggle';
import { useMutation, useApolloClient } from 'react-apollo';
import { withRouter } from 'next/router';
import { Navbar } from '../styles/Navbar';
import { Text } from '../styles/Text';
import { Button } from '../styles/Button';

const LOGOUT_MUTATION = gql`
  mutation LOGOUT_MUTATION {
    logout {
      token
    }
  }
`;

const Nav = ({ data, router: { pathname, query } }) => {
  const client = useApolloClient();
  const [dropdownToggle, setDropdownToggle] = useToggle();
  const [logoutMutation, { res }] = useMutation(LOGOUT_MUTATION);

  function logout() {
    const res = logoutMutation();
    
    if (res) {
      client.cache.reset().then(() => {
        Router.push({
          pathname: '/',
          query: { logout: 'yes' },
          shallow: true 
        });
      })
    }
  }
  return (
    <Navbar
      className='navbar navbar-expand-lg navbar-dark'
      home={pathname === '/'}
    >
      {query.logout || query.signapi === 'yes'
        ? (window.location = '/')
        : console.log('tidak')}
      <div className='row align-items-center'>
        <div className='col'>
          <img
            src='/static/logo/nyatakan_white.svg'
            className='navbar-logo'
            alt='Nyatakan Logo'
          />
        </div>
        <div className='d-none d-md-block col position-relative'>
          <input
            className='navbar-search form-control form-control-sm'
            type='search'
            placeholder='Cari merchandise, store, user disini'
            aria-label='Search'
          />
          <img
            src='/static/icons/icon_search.svg'
            className='position-absolute navbar-search--icon'
            alt=''
          />
        </div>
      </div>
      <button
        className='navbar-toggler'
        type='button'
        data-toggle='collapse'
        data-target='#navbarSupportedContent'
        aria-controls='navbarSupportedContent'
        aria-expanded='false'
        aria-label='Toggle navigation'
      >
        <span className='navbar-toggler-icon'></span>
      </button>
      <div className='collapse navbar-collapse' id='navbarSupportedContent'>
        <ul className='navbar-nav ml-auto'>
          <li className='nav-item' style={{ paddingRight: '24px' }}>
            <Button primary lg className='btn'>
              Mulai Desain
            </Button>
          </li>
          
          {data && (
            <li onClick={setDropdownToggle} className='nav-item account-profile' style={{ marginTop: '4px' }}>
              <Text md color='white' style={{ marginBottom: '0px', display: 'inline-block' }}>
                {data.me.username}
              </Text>

              <img
                src='/static/icons/icon_down.svg'
                className='pl-2 navbar-icon-down'
                alt='Icon Down'
              />
              {dropdownToggle && (
                <div className='nav-dropdown-item'>
                  <ul>
                    <li><Link href='/dashboard/account'>Lihat Dashboard</Link></li>
                    <li><Link href='/dashboard/account/edit'>Lihat Profile</Link></li>
                    <li onClick={logout}>Logout</li>
                  </ul>
                </div>
              )}
            </li>
          )}

          {!data && (
            <li className='nav-item' style={{ paddingRight: '24px' }}>
              <Link href='/login'>
                <a className='btn btn-login'>Login</a>
              </Link>
            </li>
          )}
        </ul>
      </div>
    </Navbar>
  );
};

export default withRouter(Nav);
