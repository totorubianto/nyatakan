import React from 'react';
import Sidebar from './Sidebar';
import DashboardNav from './DashboardNavbar';

function DashboardLayout(props) {
  const { data } = props;

  return (
    <div style={{ display: 'flex', width: '100%', alignItems: 'stretch' }}>
      <Sidebar />
      <div
        style={{ width: '100%', transition: 'all 0.3s', paddingLeft: '188px' }}
      >
        <DashboardNav data={data} />
        {props.children}
      </div>
    </div>
  );
}

export default DashboardLayout;
