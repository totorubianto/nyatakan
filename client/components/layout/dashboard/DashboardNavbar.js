import React from 'react';
import Router from 'next/router';
import Link from 'next/link';
import gql from 'graphql-tag';
import useToggle from '../../hooks/useToggle';
import { useMutation, useApolloClient } from 'react-apollo';
import { DashboardNavbar } from '../../styles/Navbar';
import { Text } from '../../styles/Text';

const LOGOUT_MUTATION = gql`
  mutation LOGOUT_MUTATION {
    logout {
      token
    }
  }
`;

const DashboardNav = ({ data }) => {
  const client = useApolloClient();
  const [dropdownToggle, setDropdownToggle] = useToggle();
  const [logoutMutation, { res }] = useMutation(LOGOUT_MUTATION);

  function logout() {
    const res = logoutMutation();

    if (res) {
      client.cache.reset().then(() => {
        Router.push({
          pathname: '/',
          query: { logout: 'yes' },
           shallow: true 
        });
      })
    }
  }
  
  return (
    <DashboardNavbar className='navbar navbar-expand-lg navbar-light'>
      <div className='row'>
        <div className='d-none d-md-block col position-relative'>
          <input
            className='navbar-search form-control form-control-sm'
            type='search'
            placeholder='Cari merchandise, store, user disini'
            aria-label='Search'
          />

          <img
            src='/static/icons/icon_search.svg'
            className='position-absolute navbar-search--icon'
            alt=''
          />
        </div>
      </div>

      <button
        className='navbar-toggler'
        type='button'
        data-toggle='collapse'
        data-target='#navbarSupportedContent'
        aria-controls='navbarSupportedContent'
        aria-expanded='false'
        aria-label='Toggle navigation'
      >
        <span className='navbar-toggler-icon'></span>
      </button>

      <div
        style={{ paddingTop: '24px', paddingBottom: '24px' }}
        className='collapse float-left navbar-collapse'
        id='navbarSupportedContent'
      >
        <ul className='navbar-nav ml-auto'>
          <li onClick={setDropdownToggle} className='nav-item account-profile'>
            {!data.me.profile.picture ? (
              <img
                src={`http://safetravel.id/img/default.png`}
                alt='Profile Image'
                className='rounded-circle mr-2'
                style={{ width: '37px' }}
              />
            ) : (
              <img
                src={`http://localhost:5000/img/${data.me.profile.picture}`}
                alt='Profile Image'
                className='rounded-circle mr-2'
                style={{ width: '37px' }}
              />
            )}

            <Text
              style={{ marginBottom: '0px', display: 'inline-block' }}
              md
              color='#333'
              weight='600'
            >
              {data.me.username}
            </Text>
            <img
              src='/static/icons/iconspace-down-dark.svg'
              className='pl-2 navbar-icon-down'
              alt='Icon Down'
            />
            {dropdownToggle && (
              <div className='nav-dropdown-item'>
                <ul>
                  <li><Link href='/dashboard/account'>Lihat Dashboard</Link></li>
                  <li><Link href='/dashboard/account/edit'>Lihat Profile</Link></li>
                  <li onClick={logout}>Logout</li>
                </ul>
              </div>
            )}
          </li>
        </ul>
      </div>
    </DashboardNavbar>
  );
};

export default DashboardNav;
