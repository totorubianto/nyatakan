import React from 'react';
import Link from 'next/link';
import { withRouter } from 'next/router';
import { SidebarStyles } from '../../styles/Sidebar';
import { Text } from '../../styles/Text';

function Sidebar({ router: { pathname } }) {
  const isAccount = pathname === '/dashboard/account' || pathname === '/dashboard/account/edit';
  return (
    <SidebarStyles className="fixed-top">
      <img className="sidebar-logo" src="/static/logo/nyatakan_white.svg" />

      <ul className="list-unstyled sidebar-list">
        <li>
          <Link href="/">
            <div className={`sidebar-list-link ${pathname === '/' ? 'active' : ''}`}>
              <img src="/static/icons/iconspace-home.svg" />
              <Text md color="#fff" weight="400">
                Dashboard
              </Text>
            </div>
          </Link>
        </li>

        <li>
          <Link href="/dashboard/account">
            <div className={`sidebar-list-link ${isAccount ? 'active' : ''}`}>
              <img src="/static/icons/iconspace-user.svg" />
              <Text md color="#fff" weight="400">
                Akun
              </Text>
            </div>
          </Link>
        </li>

        <li>
          <Link href="/">
            <div className={`sidebar-list-link ${pathname === '/' ? 'active' : ''}`}>
              <img src="/static/icons/iconspace-packaging.svg" />
              <Text md color="#fff" weight="400">
                Produksi
              </Text>
            </div>
          </Link>
        </li>

        <li>
          <Link href="/">
            <div className={`sidebar-list-link ${pathname === '/' ? 'active' : ''}`}>
              <img src="/static/icons/iconspace-delivery.svg" />
              <Text md color="#fff" weight="400">
                Pengiriman
              </Text>
            </div>
          </Link>
        </li>

        <li>
          <Link href="/">
            <div className={`sidebar-list-link ${pathname === '/' ? 'active' : ''}`}>
              <img src="/static/icons/iconspace-wallet.svg" />
              <Text md color="#fff" weight="400">
                Keuangan
              </Text>
            </div>
          </Link>
        </li>

        <li>
          <Link href="/">
            <div className={`sidebar-list-link ${pathname === '/' ? 'active' : ''}`}>
              <img src="/static/icons/iconspace-network.svg" />
              <Text md color="#fff" weight="400">
                Website
              </Text>
            </div>
          </Link>
        </li>
      </ul>
    </SidebarStyles>
  );
}

export default withRouter(Sidebar);
