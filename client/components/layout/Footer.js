import React from 'react';
import styled from 'styled-components';
import { Text } from '../styles/Text';

const FooterStyled = styled.footer`
  padding: 14px 120px;
  color: white;

  .footer {
    &-logo {
      width: 160px;
    }
  }

  ul {
    padding-left: 0px;
    margin-bottom: 0px;
    & li {
      list-style: none;
    }
  }

  img {
    &:hover {
      cursor: pointer;
    }
  }

  hr {
    padding-bottom: 30px;
    border-top: 1px solid rgba(226, 226, 226, 0.3);
  }

  @media (max-width: 768px) {
    padding: 14px 50px;
  }

  @media (max-width: 574px) {
    padding: 14px 20px;
  }
`;

const Footer = () => (
  <FooterStyled>
    <hr />
    <div className="d-block d-md-flex flex-row justify-content-between mb-3">
      <img src="/static/logo/nyatakan_white.svg" className="footer-logo mb-3" alt="Nyatakan Logo" />
      <div className="ml-auto mb-3">
        <ul className="d-md-flex">
          <li className="mr-5">
            <Text>Tentang</Text>
          </li>
          <li className="mr-5">
            <Text>Lowongan</Text>
          </li>
          <li className="mr-5">
            <Text>Kerjasama</Text>
          </li>
          <li className="mr-5">
            <Text>Pengadaan</Text>
          </li>
          <li className="">
            <Text>Track Order</Text>
          </li>
        </ul>
      </div>
    </div>
    <p className="mb-4">Official Merchandise and Fundraising</p>
    <div className="d-flex justify-content-between mb-2">
      <ul className="d-flex align-items-end pl-0">
        <li className="mr-4">
          <img src="/static/logo/linkedin_logo.svg" alt="Linkedin" />
        </li>
        <li className="mr-4">
          <img src="/static/logo/gmail_logo.svg" alt="Gmail" />
        </li>
        <li className="mr-4">
          <img src="/static/logo/instagram_logo.svg" alt="Instagram" />
        </li>
      </ul>
      <Text md>Jakarta, Indonesia, 2019</Text>
    </div>
  </FooterStyled>
);

export default Footer;
