import React from 'react';
import styled from 'styled-components';
import Nav from '../layout/Navbar';
import Footer from '../layout/Footer';
import { Heading, Text } from '../styles/Text';
import { Button } from '../styles/Button';
import Router from 'next/router';
import dynamic from 'next/dynamic';

// const Nav = dynamic(() => import('../layout/Navbar'), {
//   ssr: false
// });
const Wrapper = styled.div`
  .card {
    border-radius: 20px;
    background-image: linear-gradient(74deg, #ff438c, #ffd743);

    .card-white {
      box-shadow: -3px 12px 14px 0 rgba(0, 0, 0, 0.1);
      background-image: linear-gradient(0deg, #fff, #fff);
    }
  }

  .merch {
    &-img {
      width: 100%;
    }
  }

  .merch-collection {
    padding-top: 3rem !important;
  }

  .bg {
    position: relative;
    background-image: linear-gradient(207deg, #154376, #092041);
    border-radius: 90% 90% 0 0 / 10%;
  }

  .bg-top {
    position: relative;
    background-image: linear-gradient(207deg, #154376, #092041);
    height: 690px;
    border-radius: 0 0 90% 90% / 10%;
  }

  .no-wrap {
    white-space: nowrap;
  }

  @media (max-width: 1020px) {
    .col-hidden {
      display: none;
    }
  }

  @media (max-width: 768px) {
    .merch-collection {
        padding-top: 0rem !important;
      }
    }
  }
`;

function HomeWrapper({ data }) {
  return (
    <Wrapper>
      <div className='bg-top'>
        <Nav data={data} />
        <div className='container mt-5 position-relative'>
          <Heading h1 regular color='#fff'>
            <strong>Lorem Ipsum</strong> is simply dummy text of the printing
            and typesetting.
          </Heading>
          <div className='row mt-5 mb-5'>
            <div className='col-lg-8 col-md-12'>
              <img className='img-fluid' src='static/images/bitmap_3.png' />
            </div>
            <div className='col-lg-4 col-hidden'>
              <div className='col-12 m-0'>
                <img
                  style={{ height: 'auto', width: '247px' }}
                  className='img-fluid'
                  src='static/images/bitmap.png'
                />
              </div>
              <div className='col-12 mt-3'>
                <img
                  style={{ height: 'auto', width: '247px' }}
                  className='img-fluid'
                  src='static/images/bitmap_2.png'
                />
              </div>
            </div>
          </div>
        </div>
      </div>

      <div className='container merch merch-collection mt-5 position-relative'>
        <div className='d-flex mt-5 mb-5 justify-content-between'>
          <Heading h2 color='#050F28'>
            Merchandise yang lagi hits
          </Heading>
          <Button className='btn merch-see-more no-wrap' secondary sm>
            Lihat Semua
          </Button>
        </div>

        <div className='row mb-5 '>
          <div className='col-6 col-xs-6 col-sm-6 col-md-6 col-lg-3'>
            <img className='merch-img mb-3' src='static/images/cloth.png' />
            <Text className='merch-name' xl color='#050F28' weight='800'>
              Black Merch
            </Text>
            <Text className='merch-type' md color='#BDBDBD' weight='600'>
              T-Shirt
            </Text>
            <Text className='merch-creator' md color='#BDBDBD' weight='800'>
              By Toto Rubianto
            </Text>
          </div>

          <div className='col-6 col-xs-6 col-sm-6 col-md-6 col-lg-3'>
            <img className='merch-img mb-3' src='static/images/cloth_2.png' />
            <Text className='merch-name' xl color='#050F28' weight='800'>
              Nasa
            </Text>
            <Text className='merch-type' md color='#BDBDBD' weight='600'>
              T-Shirt
            </Text>
            <Text className='merch-creator' md color='#BDBDBD' weight='800'>
              By Toto Rubianto
            </Text>
          </div>

          <div className='col-6 col-xs-6 col-sm-6 col-md-6 col-lg-3'>
            <img className='merch-img mb-3' src='static/images/cloth_3.png' />
            <Text className='merch-name' xl color='#050F28' weight='800'>
              Queen of Salsa
            </Text>
            <Text className='merch-type' md color='#BDBDBD' weight='600'>
              T-Shirt
            </Text>
            <Text className='merch-creator' md color='#BDBDBD' weight='800'>
              By Toto Rubianto
            </Text>
          </div>

          <div className='col-6 col-xs-6 col-sm-6 col-md-6 col-lg-3'>
            <img className='merch-img mb-3' src='static/images/cloth_4.png' />
            <Text className='merch-name' xl color='#050F28' weight='800'>
              Yeah Buoy
            </Text>
            <Text className='merch-type' md color='#BDBDBD' weight='600'>
              T-Shirt
            </Text>
            <Text className='merch-creator' md color='#BDBDBD' weight='800'>
              By Toto Rubianto
            </Text>
          </div>
        </div>

        <div className='d-flex mt-5 mb-5 justify-content-between'>
          <Heading h2 color='#050F28'>
            New Items
          </Heading>
          <Button className='btn merch-see-more no-wrap' secondary sm>
            Lihat Semua
          </Button>
        </div>

        <div className='row mb-5 '>
          <div className='col-6 col-xs-6 col-sm-6 col-md-6 col-lg-3'>
            <img className='merch-img mb-3' src='static/images/cloth.png' />
            <Text className='merch-name' xl color='#050F28' weight='800'>
              Black Merch
            </Text>
            <Text className='merch-type' md color='#BDBDBD' weight='600'>
              T-Shirt
            </Text>
            <Text className='merch-creator' md color='#BDBDBD' weight='800'>
              By Toto Rubianto
            </Text>
          </div>

          <div className='col-6 col-xs-6 col-sm-6 col-md-6 col-lg-3'>
            <img className='merch-img mb-3' src='static/images/cloth_2.png' />
            <Text className='merch-name' xl color='#050F28' weight='800'>
              Nasa
            </Text>
            <Text className='merch-type' md color='#BDBDBD' weight='600'>
              T-Shirt
            </Text>
            <Text className='merch-creator' md color='#BDBDBD' weight='800'>
              By Toto Rubianto
            </Text>
          </div>

          <div className='col-6 col-xs-6 col-sm-6 col-md-6 col-lg-3'>
            <img className='merch-img mb-3' src='static/images/cloth_3.png' />
            <Text className='merch-name' xl color='#050F28' weight='800'>
              Queen of Salsa
            </Text>
            <Text className='merch-type' md color='#BDBDBD' weight='600'>
              T-Shirt
            </Text>
            <Text className='merch-creator' md color='#BDBDBD' weight='800'>
              By Toto Rubianto
            </Text>
          </div>

          <div className='col-6 col-xs-6 col-sm-6 col-md-6 col-lg-3'>
            <img className='merch-img mb-3' src='static/images/cloth_4.png' />
            <Text className='merch-name' xl color='#050F28' weight='800'>
              Yeah Buoy
            </Text>
            <Text className='merch-type' md color='#BDBDBD' weight='600'>
              T-Shirt
            </Text>
            <Text className='merch-creator' md color='#BDBDBD' weight='800'>
              By Toto Rubianto
            </Text>
          </div>
        </div>
      </div>

      <div style={{ top: '100px' }} className='bg position-relative'>
        <div className='container'>
          <div
            style={{ bottom: '100px' }}
            className='card container p-5 position-relative'
          >
            <div className='row'>
              <div className='d-flex flex-row'>
                <div className='d-flex-inline flex-column'>
                  <Heading h2 color='#fff'>
                    Nyatakan merchandise impianmu segera!
                  </Heading>
                  <Text className='mt-2' lg color='#fff'>
                    Coba eksplor tools super Nyatakan
                  </Text>
                  <Button secondary lg>
                    Mulai Desain
                  </Button>
                </div>

                <div className='card-deck col-hidden'>
                  <div className='card card-white'>
                    <img
                      style={{ width: '62px', height: '50px' }}
                      className='img-fluid ml-4 mt-3'
                      src='static/icons/icons-priority.svg'
                    />
                    <div className='card-body'>
                      <Heading h4 color='#050f28' bold>
                        Nyatakan Creator
                      </Heading>
                      <Text lg className='card-text mt-3'>
                        Mau buat dan jual merchandise kamu disini?
                      </Text>
                    </div>
                  </div>
                  <div className='card card-white'>
                    <img
                      style={{ width: '62px', height: '50px' }}
                      className='img-fluid ml-4 mt-3'
                      src='static/icons/icons-priority.svg'
                    />
                    <div className='card-body'>
                      <Heading h4 color='#050f28' bold>
                        Pesan Merchandise
                      </Heading>
                      <Text lg className='card-text mt-3'>
                        Mau pesan merchandise kostum yang berkualitas?
                      </Text>
                    </div>
                  </div>
                </div>
              </div>
            </div>
          </div>

          <div style={{ bottom: '50px' }} className='position-relative'>
            <div className='d-flex mt-5 mb-5 justify-content-between'>
              <Heading h2 regular color='#fff'>
                Merchandise Kostum ? Pasti <strong>Nyatakan</strong>
              </Heading>
              <Button className='btn merch-see-more no-wrap' primary sm>
                Mulai Sekarang
              </Button>
            </div>
          </div>
        </div>

        <Footer />
      </div>
    </Wrapper>
  );
}

export default HomeWrapper;
