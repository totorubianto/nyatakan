import React from 'react';
import classnames from 'classnames';
import { CheckBoxInput } from '../styles/Checkbox';

const CheckboxInput = ({ name, placeholder, value, label, error, info, type, onChange, disabled }) => {
  return (
    <div className="form-check mb-3 pl-0">
      <CheckBoxInput
        type={type}
        className={classnames('form-check-input', {
          'is-invalid': error
        })}
        placeholder={placeholder}
        name={name}
        value={value}
        onChange={onChange}
        disabled={disabled}
      />
      <label htmlFor={name} className="mb-0 form-check-label">
        {label}
      </label>
    </div>
  );
};

CheckboxInput.defaultProps = {
  type: 'checkbox'
};

export default CheckboxInput;
