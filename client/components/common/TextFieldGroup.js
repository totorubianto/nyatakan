import React from 'react';
import classnames from 'classnames';
import { FormInput, FormInputLabel } from '../styles/Form';

const TextFieldGroup = ({
  name,
  placeholder,
  className,
  value,
  defaulValue,
  label,
  error,
  info,
  type,
  onChange,
  disabled
}) => {
  return (
    <div className={'form-group mb-3 ' + className}>
      <FormInputLabel htmlFor={name} className="mb-0">
        {label}
      </FormInputLabel>
      <FormInput
        type={type}
        className={classnames('form-control form-control-sm', {
          'is-invalid': error
        })}
        placeholder={placeholder}
        name={name}
        value={value}
        defaultValue={defaulValue}
        onChange={onChange}
        disabled={disabled}
      />
      {info && <small className="form-text text-muted">{info}</small>}
      {error && <div className="invalid-feedback">{error}</div>}
    </div>
  );
};

TextFieldGroup.defaultProps = {
  type: 'text'
};

export default TextFieldGroup;
