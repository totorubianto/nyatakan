import React from 'react';
import DayPickerInput from 'react-day-picker/DayPickerInput';
import { DayPicker, FormInputLabel } from '../styles/Form';

const DatePickerGroup = ({ name, value, label, error, info, onChange }) => {
  return (
    <DayPicker className="form-group mb-3" error={error}>
      <FormInputLabel htmlFor={name} className="mb-0 d-block">
        {label}
      </FormInputLabel>

      <DayPickerInput value={value} onDayChange={onChange} />
      <img className="calendar-icon" src="/static/icons/icon_calendar.svg" />
      {info && <small className="form-text text-muted">{info}</small>}
      {error && (
        <div className="invalid-feedback" error={error}>
          {error}
        </div>
      )}
    </DayPicker>
  );
};

export default DatePickerGroup;
