import React from 'react';
import { Heading } from '../styles/Text';

const ButtonGoogle = () => {
  return (
    <button
      type="button"
      className="btn btn-md btn-block mb-3 p-2"
      style={{ borderRadius: '6px', border: 'solid 1px #e2e2e2' }}>
      <div className="d-flex justify-content-around align-items-center">
        <img src="/static/logo/google_logo.svg" className="pl-2" style={{ width: '35px' }} alt="" />
        <Heading h4 className="mb-0 w-100">
          Login dengan Google
        </Heading>
      </div>
    </button>
  );
};

const ButtonFacebook = () => {
  return (
    <button
      type="button"
      className="btn btn-primary btn-md btn-block mb-3 p-2"
      style={{ borderRadius: '6px', border: 'solid 1px #e2e2e2', backgroundColor: '#4167b2' }}>
      <div className="d-flex justify-content-around align-items-center">
        <img src="/static/logo/facebook_logo.svg" className="pl-2" style={{ width: '35px', color: 'white' }} alt="" />
        <Heading h4 className="mb-0 w-100">
          Login dengan Facebook
        </Heading>
      </div>
    </button>
  );
};

export { ButtonGoogle, ButtonFacebook };
