import React from 'react';
import classnames from 'classnames';
import { FormInpuSelect, FormInputLabel } from '../styles/Form';

const SelectInput = ({ name, value, error, info, onChange, options, label }) => {
  const selectOptions = options.map(option => (
    <option key={option.label} value={option.value} disabled={option.disabled}>
      {option.label}
    </option>
  ));
  return (
    <div className="form-group">
      <FormInputLabel htmlFor={name} className="mb-0">
        {label}
      </FormInputLabel>
      <FormInpuSelect
        className={classnames('form-control form-control-lg', {
          'is-invalid': error
        })}
        name={name}
        value={value}
        onChange={onChange}>
        {selectOptions}
      </FormInpuSelect>
      {info && <small className="form-text text-muted">{info}</small>}
      {error && <div className="invalid-feedback">{error}</div>}
    </div>
  );
};

export default SelectInput;
