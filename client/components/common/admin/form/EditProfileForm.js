import React, { useState } from 'react';
import gql from 'graphql-tag';
import { useMutation } from 'react-apollo';
import { CURRENT_USER_QUERY } from '../../../../lib/checkLoggedIn';
import { ValidateUpdateprofile } from '../../../hooks/useInputValidate';
import { Button } from '../../../styles/Button';
import TextFieldGroup from '../../TextFieldGroup';
import useForm from '../../../hooks/useForm';

const UPDATE_PROFILE_MUTATION = gql`
  mutation UPDATE_PROFILE_MUTATION($username: String, $fullname: String, $email: String, $phone: String) {
    updateUser(user: { username: $username, fullname: $fullname, email: $email, phone: $phone }) {
      errors {
        value
        param
      }
    }
  }
`;

export default function EditProfileForm({ dataProfile }) {
  const onCompleted = data => {
    if (!data.updateUser.errors.length > 0) {
      setSuccess('Sudah terupdate');
    }
  };

  const onError = error => {
    // If you want to send error to external service?
    console.error(error);
  };

  const [success, setSuccess] = useState();
  const { values, handleChange, errors, handleSubmit } = useForm(updateProfile, ValidateUpdateprofile);
  const [updateProfileMutation, { data, loading, error }] = useMutation(UPDATE_PROFILE_MUTATION, {
    onCompleted,
    onError
  });

  async function updateProfile() {
    await updateProfileMutation({
      variables: { username: values.username, fullname: values.fullname, email: values.email, phone: values.phone },
      refetchQueries: [{ query: CURRENT_USER_QUERY }]
    });
  }

  return (
    <form onSubmit={handleSubmit}>
      <div className="form-row">
        <TextFieldGroup
          name="username"
          placeholder={dataProfile.me.username}
          label="Username"
          className="col-md mr-md-2"
          value={values.username || ''}
          onChange={handleChange}
          error={
            errors.username ||
            (data &&
              data.updateUser.errors.length > 0 &&
              data.updateUser.errors[0].param === 'username' &&
              data.updateUser.errors[0].value)
          }
        />
        <TextFieldGroup
          name="fullname"
          placeholder={dataProfile.me.profile.fullname}
          label="Nama lengkap"
          className="col-md"
          value={values.fullname || ''}
          onChange={handleChange}
          error={
            errors.fullname ||
            (data &&
              data.updateUser.errors.length > 0 &&
              data.updateUser.errors[0].param === 'fullname' &&
              data.updateUser.errors[0].value)
          }
        />
      </div>
      <div className="form-row">
        <TextFieldGroup
          name="email"
          placeholder={dataProfile.me.email}
          label="Email"
          className="col-md mr-md-2"
          value={values.email || ''}
          onChange={handleChange}
          error={
            errors.email ||
            (data &&
              data.updateUser.errors.length > 0 &&
              data.updateUser.errors[0].param === 'email' &&
              data.updateUser.errors[0].value)
          }
        />
        <TextFieldGroup
          name="userclass"
          placeholder={dataProfile.me.profile.userclass}
          label="Role/Jabatan"
          className="col-md"
          disabled={true}
        />
      </div>
      <div className="form-row mb-2">
        <TextFieldGroup
          name="phone"
          placeholder={dataProfile.me.profile.phone}
          label="Nomor HP"
          className="col-md-6 "
          value={values.phone || ''}
          onChange={handleChange}
          error={
            errors.phone ||
            (data &&
              data.updateUser.errors.length > 0 &&
              data.updateUser.errors[0].param === 'phone' &&
              data.updateUser.errors[0].value)
          }
        />
      </div>
      <Button type="submit" primary lg>
        Simpan Perubahan
      </Button>
      <span className="text-success"> {success}</span>
    </form>
  );
}
