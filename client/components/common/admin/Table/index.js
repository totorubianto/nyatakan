import React from 'react'
import { StyledTable } from '../../../styles/StyledTable';

const Table = ({ children }) => {
    return (
        <div className="overflow-auto">
            <StyledTable>
                { children }
            </StyledTable>
        </div>
    )
}

const TableBody = () => {}

export default Table

