import React from 'react';
import { Text } from '../../../styles/Text';
import { MiddleText,
         VerticalImageWithText,
         SocialMediaButtonList, } from '../../../common/admin/Table/components';

const VerifTableHead = [
    {key: 0, title: "AKUN"},
    {key: 1, title: "TANGGAL REQUEST"},
    {key: 2, title: "EMAIL"},
    {key: 3, title: "NOMOR HP"},
    {key: 4, title: "SOCIAL MEDIA"},
    {key: 6, title: ""},
];

function VerifTableContent() {
    return (
        <React.Fragment>
            <thead>
                <tr>
                    {VerifTableHead.map((table) => {
                        return(<th key={table.key} scope="col"><Text sm color="#050f28" weight="bold">{ table.title }</Text></th>);
                    })}
                </tr>
            </thead>

            <tbody>
                <tr>
                    <td><VerticalImageWithText imageName="oval.png" text="Yessica Tamara"/></td>
                      <td><MiddleText text="21 Agustus 2019" weight="400" /></td>
                      <td><MiddleText text="chika.tamara@gmail.com" weight="400"/></td>   
                      <td><MiddleText text="089941568121" weight="800"/></td>
                      <td><SocialMediaButtonList /></td>
                      <td></td>
                </tr>
            </tbody>
        </React.Fragment>
    )
}

export default VerifTableContent;
