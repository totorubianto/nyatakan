import React from 'react';
import styled from 'styled-components';
import { Text } from '../../../../styles/Text';

const Wrapper = styled.div`
    height: 48px;

    .content-img {
        width: 48px;
        height: 48px;
        margin-right: 16px;
        border-radius: 50%;

        & img {
            object-fit: contain; 
        }
    }

    .row {
        margin: 0px;
    }

    & p {
        margin-bottom: 0px;
    }

    & ul {
        list-style: none;
        margin: 0px;
        padding: 0px;

        & li {
            display: block;
            float: left;
            margin-right: 1px;
            margin-bottom: 8px;

            & a {
                cursor: pointer;
            }

            & .card {
                width: 40px;
                height: 40px;
                border-radius: 6px;
                box-shadow: 0 1px 8px 0 rgba(0, 0, 0, 0.1);
                padding: 8px !important;
            }
        }
    }

    .store-list-img {
        width: 32px;
        height: 32px;
        border-radius: 50%;
    }

    @media (max-width: 1278px) {
        height: auto;
    }
`
const MiddleText = ({ text, weight }) => {
    return (
        <Wrapper className="row align-items-center">
            <div>
                <Text md color="#092041" weight={ weight }>{ text }</Text>
            </div>
        </Wrapper>
    )
}

const MiddleDoubleText = ({ firstText, secondText }) => {
    return (
        <Wrapper className="row align-items-center">
            <div>
                <Text md color="#092041" weight="800">{ firstText }</Text>
                <Text md color="#bdbdbd" weight="400">{ secondText }</Text>
            </div>
        </Wrapper>
    )
}

const VerticalImageWithText = ({ imageName, text }) => {
    let src = "";

    if(imageName) {
        src = `http://localhost:5000/img/${imageName}`;
    } else {
        src = `http://safetravel.id/img/default.png`;
    }
    

    return (
        <Wrapper className="row flex-column flex-md-row align-items-center">
            <div style={{ background: `url(${src}) center center / 100% 100% no-repeat` }} className="content-img"></div>
            <div>
                <Text md color="#092041" weight="800">{ text }</Text>
            </div>
        </Wrapper>
    )
}

const VerticalImageWithDoubleText = ({ imageName, firstText, secondText }) => {
    let src = "";

    if(imageName) {
        src = `/static/store-images/${imageName}`;
    } else {
        src = `http://safetravel.id/img/default.png`;
    }

    return (
        <Wrapper className="row align-items-center">
            <div style={{ background: `url(${src}) center center / 100% 100% no-repeat` }} className="content-img" />
            <div>
                <Text md color="#092041" weight="800">{ firstText }</Text>
                <Text md color="#bdbdbd" weight="400">{ secondText }</Text>
            </div>
        </Wrapper>
    )
}

const SocialMediaButtonList = ({ youtube, instagram, twitter }) => {

    return (
        <Wrapper className="row align-items-center">
            <ul>
                {youtube && (
                    <li>
                        <a href={ youtube }>
                            <div className="card align-items-center">
                                <img src="/static/icons/icon-instagram.svg" />
                            </div>
                        </a>
                    </li>
                )}
                
                {instagram && (
                    <li>
                        <a href={ instagram }>
                            <div className="card align-items-center">
                                <img src="/static/icons/icon-instagram.svg" />
                            </div>
                        </a>
                    </li>
                )}

                {twitter && (
                    <li>
                        <a href={ twitter }>
                            <div className="card align-items-center">
                                <div><img src="/static/icons/icon-twitter.svg" /></div>
                            </div>
                        </a>
                    </li>
                )}
            </ul>
        </Wrapper>
    )
}

const StoreList = ({ storeList }) => {
    const src = "/static/store-images/";

    return (
        <Wrapper className="row align-items-center">
            <ul>
                {/* {storeList ?
                    storeList.slice(0, 4).map((store) => {

                        return(
                            <li>
                                <a href={ youtube }>
                                    <div style={{ backgroundImage: `${src}${store}` }} className="content-img align-items-center"></div>
                                </a>
                            </li>
                        );
                    }) : <Text md color="#092041" weight="600">Belum Meliliki Store</Text>
                } */}
                <li>
                    <div style={{ background: `url(${src}store-1.png) center center no-repeat`}} className="store-list-img align-items-center"></div>
                </li>
                <li>
                    <div style={{ background: `url(${src}store-2.png) center center no-repeat` }} className="store-list-img align-items-center"></div>
                </li>
                <li>
                    <div style={{ background: `url(${src}store-3.png) center center no-repeat` }} className="store-list-img align-items-center"></div>
                </li>
                <li>
                    <div style={{ background: `url(${src}store-4.png) center center no-repeat` }} className="store-list-img align-items-center"></div>
                </li>
            </ul>
        </Wrapper>
    )
}

export{ 
    MiddleText,
    MiddleDoubleText,
    VerticalImageWithText,
    VerticalImageWithDoubleText,
    SocialMediaButtonList,
    StoreList,
}

