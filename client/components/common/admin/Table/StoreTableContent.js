import React from 'react';
import { Text } from '../../../styles/Text';
import { MiddleText,
         MiddleDoubleText,
         VerticalImageWithDoubleText} from '../../../common/admin/Table/components';

const StoreTableHead = [
    {key: 0, title: "STORE"},
    {key: 1, title: "JUMLAH KOLEKSI"},
    {key: 2, title: "PRODUK TERJUAL"},
    {key: 3, title: "GMV"},
    {key: 4, title: "JUMLAH KUNJUNGAN"},
    {key: 6, title: ""},
];

function StoreTableContent({ content }) {
    console.log(content);
    return (
        <React.Fragment>
            <thead>
                <tr>
                    {StoreTableHead.map((table) =>
                        (<th key={table.key} scope="col"><Text sm color="#050f28" weight="bold">{ table.title }</Text></th>)
                    )}
                </tr>
            </thead>

            <tbody>
                { content.map((data) =>
                    data.profile !== null && (
                        <tr key={data._id}>
                            <td><VerticalImageWithDoubleText imageName={data.cover} firstText={data.store_name} secondText={data.profile.fullname}/></td>
                            <td><MiddleDoubleText firstText="- Koleksi" secondText="- Produk" /></td>
                            <td><MiddleText text="- pcs" weight="400"/></td>
                            <td><MiddleText text="Rp 1.000.000" weight="800"/></td>
                            <td><MiddleText text="1.500 Kunjungan" weight="400"/></td>
                            <td></td>
                        </tr>
                    )
                )}
            </tbody>
        </React.Fragment>
    )
}

export default StoreTableContent;
