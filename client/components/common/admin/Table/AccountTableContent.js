import React from 'react';
import { Text } from '../../../styles/Text';
import { MiddleText, VerticalImageWithText, StoreList } from './components';

const AccountTableHead = [
  { key: 0, title: 'AKUN' },
  { key: 1, title: 'JENIS AKUN' },
  { key: 2, title: 'STORE' },
  { key: 3, title: 'JUMLAH KOLEKSI' },
  { key: 4, title: 'GMV' },
  { key: 5, title: 'TERAKHIR ONLINE' },
  { key: 6, title: '' }
];

const AccountTableContent = ({ content }) => {
  return (
    <React.Fragment>
      <thead>
        <tr>
          {AccountTableHead.map(table => (
            <th key={table.key} scope="col">
              <Text sm color="#050f28" weight="bold">
                {table.title}
              </Text>
            </th>
          ))}
        </tr>
      </thead>

      <tbody>
        {content.map(data => (
          <tr>
            <td>
              <VerticalImageWithText imageName={data.picture} text={data.fullname} />
            </td>
            <td>
              <MiddleText text={data.userclass} weight="400" />
            </td>
            <td>
              <StoreList />
            </td>
            <td>
              <MiddleText text="3 Koleksi" weight="400" />
            </td>
            <td>
              <MiddleText text="Rp 1.000.000" weight="800" />
            </td>
            <td>
              <MiddleText text="21 Agustus 2019" weight="400" />
            </td>
            <td></td>
          </tr>
        ))}
      </tbody>
    </React.Fragment>
  );
};

export default AccountTableContent;
