import React from 'react';
import classnames from 'classnames';
import { FormInput, FormInputLabel, FormPasswordLabel, PasswordVisibleIcon } from '../styles/Form';

const PasswordTextField = ({
  name,
  placeholder,
  value,
  label,
  error,
  info,
  type,
  onChange,
  toggleVisible,
  forgetPassword,
  disabled
}) => {
  return (
    <div className="form-group mb-3 position-relative">
      <div className="d-flex justify-content-between">
        <FormInputLabel htmlFor={name} className="mb-0">
          {label}
        </FormInputLabel>
        {forgetPassword && <FormPasswordLabel className="mb-0">Lupa Password?</FormPasswordLabel>}
      </div>
      <FormInput
        type={type}
        className={classnames('form-control form-control-sm', {
          'is-invalid': error
        })}
        placeholder={placeholder}
        name={name}
        value={value}
        onChange={onChange}
        disabled={disabled}
      />
      {value.length > 0 && (
        <PasswordVisibleIcon
          className="position-absolute"
          src="/static/icons/icons_visible.svg"
          alt="Visible Password"
          onClick={toggleVisible}
        />
      )}
      {info && <small className="form-text text-muted">{info}</small>}
      {error && <div className="invalid-feedback">{error}</div>}
    </div>
  );
};

PasswordTextField.defaultProps = {
  type: 'password',
  forgetPassword: true
};

export default PasswordTextField;
