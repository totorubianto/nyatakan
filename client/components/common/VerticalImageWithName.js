import React from 'react';
import styled from 'styled-components';
import { Text } from '../styles/Text';

const Wrapper = styled.div`
    height: 48px;

    .content-img {
        width: 48px;
        height: 48px;

        & img {
            object-fit: contain; 
        }
    }

    & p {
        margin-bottom: 0px;
    }
`

function VerticalImageWithName({ src, img_name }) {
    return (
        <Wrapper className="row align-items-center">
            <div className="content-img">
                <img src="oval.png"/>
            </div>
            <div>
                <Text md color="#092041" weight="400">Yessica Tamara</Text>
            </div>
        </Wrapper>
    )
}

export default VerticalImageWithName;
