import styled, { css } from 'styled-components';

export const Text = styled.p`
  font-family: "Open Sans", sans-serif;
  font-weight: ${props => props.weight};
  color: ${props => props.color};
  opacity: ${props => props.opacity};
  vertical-align: middle;
  /* opacity: ${props => props.opacity || 1} ; */

  ${props =>
    props.xl &&
    css`
      line-height: 27px;
      font-size: 20px;
    `}

  ${props =>
    props.lg &&
    css`
      line-height: 22px;
      font-size: 16px;
    `}

  ${props =>
    props.md &&
    css`
      line-height: 18px;
      font-size: 14px;
    `}

  ${props =>
    props.sm &&
    css`
      line-height: 16px;
      font-size: 12px;
    `}
`;

export const Heading = styled.span`
  font-family: "Open Sans", sans-serif;
  color: ${props => props.color};
  vertical-align: middle;

  ${props =>
    props.h1 &&
    css`
      line-height: 65px;
      font-size: 48px;

      @media (max-width: 768px) {
        line-height: 43px;
        font-size: 32px;
      }
    `}

  ${props =>
    props.h2 &&
    css`
      line-height: 43px;
      font-size: 32px;

      @media (max-width: 768px) {
        line-height: 26px;
        font-size: 20px;
      }
    `}

  ${props =>
    props.h3 &&
    css`
      line-height: 26px;
      font-size: 20px;

      @media (max-width: 574px) {
        line-height: 22px;
        font-size: 16px;
      }
    `}

    ${props =>
      props.h4 &&
      css`
        line-height: 22px;
        font-size: 16px;
      `}

    ${props =>
      props.bold &&
      css`
        font-weight: 800;
      `}
    ${props =>
      props.semi_bold &&
      css`
        font-weight: 600;
      `}
    ${props =>
      props.regular &&
      css`
        font-weight: 400;
      `}
`;
