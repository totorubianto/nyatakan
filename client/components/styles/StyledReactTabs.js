import styled from "styled-components";
import { Tab, Tabs, TabList, TabPanel } from 'react-tabs';

const StyledTabs = styled(Tabs)`
    margin-top: 20px;
`

const StyledTabList = styled(TabList)`
    display: flex;
    flex-warp: warp;
    list-style: none;
    padding-left: 0;
    list-style: none;
    border-bottom: 1px solid #dee2e6;
`

StyledTabList.tabsRole = 'TabList';

const StyledTab = styled(Tab)`
    cursor: pointer;
    margin-bottom: -1px;
    padding: .5rem 1.5rem !important;

    &.active {
        border-bottom: 4px solid #4380ff;

        & a {
            color: #092041;
        }
    }

    & a {
        color: #bdbdbd;
        border: none;
        background-color: transparent;
    }
`

StyledTab.tabsRole = 'Tab';

export {
    StyledTabs,
    StyledTabList,
    StyledTab,
}