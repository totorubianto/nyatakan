import styled, { css } from 'styled-components';

export const Navbar = styled.nav`
  padding: 24px 0 20px 0;
  margin: 0px 120px;
  ${props =>
    props.home &&
    css`
      border: 0;
      border-bottom: 1px solid rgba(255, 255, 255, 0.2);
    `}

  .navbar {
    &-border {
    }
    &-logo {
      width: 130px;
    }

    &-icon {
      &-down {
        width: 22px;

        :hover {
          cursor: pointer;
        }
      }
    }

    &-search {
      width: 290px;
      height: 36px;
      padding-left: 35px;
      border-radius: 6px;
      border: solid 1px #e2e2e2;
      background-color: #ffffff;

      &::placeholder {
        opacity: 0.6;
        font-family: 'Open Sans', sans-serif;
        border-radius: 6px;
        color: #092041;
      }

      &--icon {
        width: 12px;
        top: 12px;
        left: 30px;
      }
    }
  }

  .btn-login {
    width: 131px;
    cursor: pointer;
    font-family: Open Sans, sans-sherif;
    text-align: center;
    font-size: 16px;
    line-height: 1.25;
    border-radius: 6px;
    border: solid 1px #ffffff;
    padding: 9px 16px;
    color: #fff;
    background-color: transparent;
  }

  .account-profile {
    &:hover {
      cursor: pointer;
    }
  }

  .nav-dropdown-item {
    margin-top: 10px;
    position: absolute;
    z-index: 9999;

    & ul {
      list-style: none;
      background: #fff;
      padding-left: 0px;
      box-shadow: 0 1px 6px 0 rgba(49, 53, 59, 0.6);

      & li {
        padding: 10px 12px;

        & a {
          text-decoration: none;
          color: #333;
        }
      }
    }
  }

  @media (max-width: 1024px) {
    .navbar-search {
      width: 225px;
    }
  }

  @media (max-width: 768px) {
    padding: 14px 50px;
    .navbar-search {
      width: 290px;
    }
  }

  @media (max-width: 574px) {
    padding: 14px 20px;
  }
`;

export const DashboardNavbar = styled(Navbar)`
  border-bottom: solid 1px #e2e2e2;
  background-color: #ffffff;
  padding: 0 32px;
  margin: 0px;
`;
