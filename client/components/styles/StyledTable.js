import styled from "styled-components";

export const StyledTable = styled.table`
    width: 100%;
    margin-bottom: 1rem;
    color: #212529;

    & thead {
        border-radius: 50px;
        background-color: #f4f4f4;

        & tr {
            border-bottom: none;

            & p {
                opacity: 0.5;
                margin-bottom: 0px;
            }
        }
    }
    
    & tr {
        border-bottom: 1px solid #e2e2e2;
        padding: .75rem;
        vertical-align: top;

        & p {
            opacity: 0.5;
            margin-bottom: 0px;
        }
    }

    & th, & td {
        padding: .75rem;
        vertical-align: top;
    }
`;