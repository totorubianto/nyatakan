import styled, { css } from 'styled-components';

export const DayPicker = styled.div`
  position: relative;

  .DayPickerInput {
    width: 100%;

    input {
      width: 100%;
      border-radius: 6px;
      border: solid 1px #e2e2e2;
      background-color: #ffffff;
      padding: 4px 16px;
      font-size: 16px;

      ${props =>
        props.error &&
        css`
          border-color: #dc3545;
          padding-right: calc(1.5em + 0.75rem);
        `}
    }
  }

  .invalid-feedback {
    ${props =>
      props.error &&
      css`
        display: block;
      `}
  }

  .calendar-icon {
    position: absolute;
    top: 29px;
    right: 12px;

    &:hover {
      cursor: pointer;
    }
  }
`;

export const FormInpuSelect = styled.select`
  border-radius: 6px;
  border: solid 1px #e2e2e2;
  background-color: #ffffff;
  font-size: 16px;
  background: url('/static/icons/icon_down_pink.svg') no-repeat right;
  background-position-x: 385px;
  -webkit-appearance: none;

  &:focus {
    box-shadow: none;
  }

  &::placeholder {
    opacity: 0.6;
    font-family: 'Open Sans', sans-serif;
    font-size: 14px;
    font-weight: normal;
    font-style: normal;
    font-stretch: normal;
    line-height: 1.29;
    letter-spacing: normal;
    color: #092041;
  }

  @media (max-width: 768px) {
    background-position-x: 590px;
  }

  @media (max-width: 426px) {
    background-position-x: 290px;
  }

  @media (max-width: 376px) {
    background-position-x: 240px;
  }
`;

export const FormInput = styled.input`
  border-radius: 6px;
  border: solid 1px #e2e2e2;
  background-color: #ffffff;
  padding: 16px;
  font-size: 16px;

  &:focus {
    box-shadow: none;
  }

  &::placeholder {
    opacity: 0.6;
    font-family: 'Open Sans', sans-serif;
    font-size: 14px;
    font-weight: normal;
    font-style: normal;
    font-stretch: normal;
    line-height: 1.29;
    letter-spacing: normal;
    color: #092041;
  }
`;

export const FormInputLabel = styled.label`
  opacity: 0.6;
  font-family: 'Open Sans', sans-serif;
  font-size: 14px;
  font-weight: 600;
  font-style: normal;
  font-stretch: normal;
  line-height: 1.29;
  letter-spacing: normal;
  color: #092041;
  padding-bottom: 4px;
`;

export const FormPasswordLabel = styled.span`
  font-family: 'Open Sans', sans-serif;
  font-size: 14px;
  font-weight: 600;
  font-style: normal;
  font-stretch: normal;
  line-height: 1.29;
  letter-spacing: normal;
  color: #4380ff;
  padding-bottom: 4px;

  &:hover {
    cursor: pointer;
  }
`;

export const PasswordVisibleIcon = styled.img`
  top: 32px;
  right: 35px;

  &:hover {
    cursor: pointer;
  }
`;
