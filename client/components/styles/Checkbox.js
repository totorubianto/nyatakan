import styled from 'styled-components';

export const CheckBoxInput = styled.input`
  position: absolute;
  left: 25px;
  z-index: 99;
  top: 4px;
  opacity: 0;
  padding-left: 0;

  &:hover {
    cursor: pointer;
  }

  & + label {
    position: relative;
    cursor: pointer;
    font-family: 'Open Sans', sans-serif;
    font-size: 12px;
    line-height: 1.5;
    letter-spacing: normal;
    color: #092041;
    vertical-align: middle;
  }

  & + label:before {
    content: '';
    margin-right: 10px;
    border-radius: 6px;
    border: solid 1px #e2e2e2;
    background-color: #ffffff;
    display: inline-block;
    vertical-align: text-top;
    width: 20px;
    height: 20px;
    background: white;
    transition: 0.3s;
  }

  &:hover:disabled + label:before {
    border: none;
    transition: 0.3s;
  }

  &:hover + label:before {
    border: solid 1px #ff438c;
    transition: 0.3s;
  }

  &:focus + label:before {
    transition: 0.3s;
  }

  &:checked + label:before {
    background: #ff438c;
  }

  &:disabled + label {
    color: #b8b8b8;
    cursor: auto;
  }

  &:disabled + label:before {
    box-shadow: none;
    background: #ddd;
  }

  &:checked + label:after {
    content: '';
    position: absolute;
    left: 5px;
    top: 9px;
    background: white;
    width: 2px;
    height: 2px;
    box-shadow: 2px 0 0 white, 4px 0 0 white, 4px -2px 0 white, 4px -4px 0 white, 4px -6px 0 white, 4px -8px 0 white;
    transform: rotate(45deg);
  }
`;
