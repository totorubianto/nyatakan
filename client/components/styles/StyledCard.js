import styled, { css } from 'styled-components';

const Card = styled.div`
    border: none;
    border-radius: 20px;
    background-color: #fff;
    box-shadow: 0 1px 6px 0 rgba(49, 53, 59, 0.12);
`

const CardBody = styled.div`
    padding: 24px;
    
    .card-body-image {
        width: 140px;
        height: 140px;
    }
`

export {
    Card,
    CardBody
}