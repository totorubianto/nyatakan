import styled, { css } from 'styled-components';

export const Button = styled.button`
  border-radius: 6px;
  border-style: none;
  line-height: 1.25;
  text-align: center;
  font-weight: 600;
  cursor: pointer;
  font-family: Open Sans, sans-sherif;

  ${props =>
    props.primary &&
    css`
      background-color: #ff438c;
      color: #ffffff;

      &:hover {
        background-color: #dc3676;
        color: #ffffff;
      }
    `}

  ${props =>
    props.secondary &&
    css`
      border: solid 1px #ff438c;
      background-color: #ffffff;
      color: #ff438c;

      &:hover {
        background-color: #ff438c;
        color: #ffffff;
      }
    `}

    ${props =>
      props.transparent &&
      css`
        border: solid 1px #ffffff;
        background-color: transparent;
        color: #ffffff;

        &:hover {
          border: solid 1px #ff438c;
          background-color: #ff438c;
          color: #ffffff;
        }
      `}

  ${props =>
    props.disable &&
    css`
      background-color: #bdbdbd;
      color: #ffffff;
    `}

  ${props =>
    props.lg &&
    css`
      padding: 10px 16px;
      font-size: 16px;
    `}

  ${props =>
    props.sm &&
    css`
      padding: 9px 12px;
      font-size: 14px;
    `}
`;
