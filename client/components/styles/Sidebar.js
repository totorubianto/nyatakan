import styled from 'styled-components';

export const SidebarStyles = styled.nav`
  width: 188px;
  height: 100vh;
  background-image: linear-gradient(181deg, #154376, #092041);
  padding-top: 32px;

  .active {
    opacity: 1 !important;
  }

  .sidebar-logo {
    width: 120px;
    height: 24px;
    margin: 0px 32px;
    cursor: pointer;
  }

  .sidebar-list {
    margin-top: 35px;

    & li {
      padding: 11px 0px 11px 32px;
      cursor: pointer;

      & p {
        display: inline-block;
        margin-left: 8px;
        margin-bottom: 0px;
      }

      &:hover {
        border-left: 4px solid #4380ff;
        background-color: rgba(255, 255, 255, 0.05);

        & p {
          display: inline-block;
          margin-left: 8px;
          margin-bottom: 0px;
        }
      }
    }

    &-link {
      opacity: 0.6;
    }
  }

  @media (max-width: 1024px) {
  }

  @media (max-width: 768px) {
  }

  @media (max-width: 574px) {
  }
`;
