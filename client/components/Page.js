import React from 'react';
import Meta from '../components/Meta';

const Page = props => {
  return (
    <>
      <Meta />
      {props.children}
    </>
  );
};

export default Page;
