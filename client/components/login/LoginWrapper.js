import React from 'react';
import Link from 'next/link';
import styled from 'styled-components';
import LoginForm from './components/LoginForm';
import { FacebookLoginForm, GoogleLoginForm } from './components/ThirdPartyLoginForm';
import { Heading, Text } from '../styles/Text';

const Wrapper = styled.div`
  width: 510px;
  border-radius: 20px;
  background-color: #ffffff;
  margin-top: 60px;
  margin-bottom: 40px;

  .login {
    &-header {
      padding: 32px 32px 16px;
    }

    &-wrapper {
      padding: 0 32px;
    }

    &-arrow {
      padding-right: 16px;
      &:hover {
        cursor: pointer;
      }
    }
  }

  .recaptcha {
    margin: auto;
  }

  .daftar-link {
    font-family: 'Open Sans', sans-serif;
    font-size: 14px;
    font-weight: normal;
    font-style: normal;
    font-stretch: normal;
    line-height: 1.29;
    letter-spacing: normal;
    color: #4380ff !important;

    &:hover {
      cursor: pointer;
      color: #4380ff !important;
    }
  }

  @media (max-width: 768px) {
    width: auto;
    margin-top: 40px;
    margin-bottom: 20px;
  }

  @media (max-width: 576px) {
    width: auto;
    margin-right: 1%;
    margin-left: 1%;
  }
`;

function LoginWrapper() {
  return (
    <Wrapper className="container">
      <div className="login-header d-flex">
        <img src="/static/icons/icons_arrow_left.svg" className="login-arrow" alt="Back" />
        <Heading h3 color="#333">
          Login
        </Heading>
      </div>
      <div className="login-wrapper pb-4">
        <LoginForm />
        <div className="d-flex align-items-center mb-2">
          <hr className="w-100" />
          <Text sm color="#092041" className="mx-3 mb-0">
            atau
          </Text>
          <hr className="w-100" />
        </div>
        <FacebookLoginForm />
        <GoogleLoginForm />

        <div className="text-center">
          <span>
            Belum punya akun?{' '}
            <Link href="/signup" prefetch={false}>
              <a className="daftar-link">Daftar</a>
            </Link>
          </span>
        </div>
      </div>
    </Wrapper>
  );
}

export default LoginWrapper;
