import React from 'react';
import { useMutation, useApolloClient } from '@apollo/react-hooks';
import gql from 'graphql-tag';
import cookie from 'cookie';
import redirect from '../../../lib/redirect';
import useForm from '../../hooks/useForm';
import { ValidateSigin } from '../../hooks/useInputValidate';
import useToggle from '../../hooks/useToggle';
import TextFieldGroup from '../../common/TextFieldGroup';
import PasswordTextField from '../../common/PasswordTextField';
import CheckboxInput from '../../common/CheckboxInput';

const SIGN_IN = gql`
  mutation SIGN_IN($email: String!, $password: String!) {
    signInUser(user: { email: $email, password: $password }) {
      token
      errors {
        value
        param
      }
    }
  }
`;

export default function LoginForm() {
  const client = useApolloClient();

  const onCompleted = data => {
    if (!data.signInUser.errors.length > 0) {
      // Store the token in cookie
      document.cookie = cookie.serialize('token', data.signInUser.token, {
        sameSite: true,
        path: '/',
        maxAge: 30 * 24 * 60 * 60 // 30 days
      });
      // Force a reload of all the current queries now that the user is
      // logged in
      client.cache.reset().then(() => {
        redirect({}, '/');
      });
    }
  };

  const onError = error => {
    // If you want to send error to external service?
    console.error(error);
  };

  const { values, handleChange, errors, handleSubmit } = useForm(login, ValidateSigin);
  const [save, toggleIsSave] = useToggle(true);
  const [visible, toggleIsVisible] = useToggle(false);
  const [signinMutation, { data, loading }] = useMutation(SIGN_IN, {
    onCompleted,
    onError
  });

  async function login() {
    await signinMutation({
      variables: { email: values.email, password: values.password }
    });
  }

  return (
    <form onSubmit={handleSubmit}>
      <TextFieldGroup
        name="email"
        placeholder="Masukkan Email atau Username"
        label="Email / Username"
        value={values.email || ''}
        onChange={handleChange}
        error={
          errors.email ||
          (data &&
            data.signInUser.errors.length > 0 &&
            data.signInUser.errors[0].param === 'email' &&
            data.signInUser.errors[0].value)
        }
      />
      <PasswordTextField
        type={visible ? 'text' : 'password'}
        name="password"
        placeholder="Masukkan Password"
        label="Password"
        value={values.password || ''}
        onChange={handleChange}
        toggleVisible={toggleIsVisible}
        error={
          errors.password ||
          (data &&
            data.signInUser.errors.length > 0 &&
            data.signInUser.errors[0].param === 'password' &&
            data.signInUser.errors[0].value)
        }
      />
      <CheckboxInput
        className="mb-3"
        name="save"
        type="checkbox"
        label="Simpan password"
        checked={save}
        onChange={toggleIsSave}
      />
      <button
        type="submit"
        className="btn btn-primary btn-md btn-block border-0 mb-2 p-2"
        style={{ backgroundColor: '#ff438c' }}>
        Login
      </button>
    </form>
  );
}
