import React from 'react';
import Router from 'next/router';
import gql from 'graphql-tag';
import cookie from 'cookie';
import redirect from '../../../lib/redirect';
import { useMutation, useApolloClient } from 'react-apollo';
import { CURRENT_USER_QUERY } from '../../../lib/checkLoggedIn';
import { ButtonFacebook, ButtonGoogle } from '../../common/CostumButton';
import FacebookLogin from 'react-facebook-login/dist/facebook-login-render-props';
import { GoogleLogin } from 'react-google-login';
import { join } from 'path';

const SIGNIN_MUTATION = gql`
  mutation SIGNIN_MUTATION($email: String!) {
    signApi(email: $email) {
      token
      errors {
        value
        param
      }
    }
  }
`;

const addToken = res => {
  document.cookie = cookie.serialize('token', res.data.signApi.token, {
    sameSite: true,
    path: '/',
    maxAge: 30 * 24 * 60 * 60 // 30 days
  });

  Router.push({
    pathname: '/',
    query: { signapi: 'yes' }
  });
};

const FacebookLoginForm = () => {
  const client = useApolloClient();
  const [thirdPartySignInMutation] = useMutation(SIGNIN_MUTATION);

  const responseFacebook = async response => {
    const res = await thirdPartySignInMutation({
      variables: { email: response.email }
    });
    if (res) {
      // Store the token in cookie
      document.cookie = cookie.serialize('token', res.data.signApi.token, {
        sameSite: true,
        path: '/',
        maxAge: 30 * 24 * 60 * 60 // 30 days
      });
      // Force a reload of all the current queries now that the user is
      // logged in
      client.cache.reset().then(() => {
        redirect({}, '/signup');
      });
    }
  };

  return (
    <FacebookLogin
      // appId="375634773349026"
      appId="394820107870866"
      fields="name,email,picture"
      callback={responseFacebook}
      render={renderProps => (
        <div onClick={renderProps.onClick}>
          <ButtonFacebook />
        </div>
      )}
    />
  );
};

const GoogleLoginForm = () => {
  const client = useApolloClient();
  const [thirdPartySignInMutation] = useMutation(SIGNIN_MUTATION);

  const responseGoogleOnSuccess = async response => {
    const res = await thirdPartySignInMutation({
      variables: { email: response.profileObj.email }
    });

    if (res) {
      // Store the token in cookie
      document.cookie = cookie.serialize('token', res.data.signApi.token, {
        sameSite: true,
        path: '/',
        maxAge: 30 * 24 * 60 * 60 // 30 days
      });
      // Force a reload of all the current queries now that the user is
      // logged in
      client.cache.reset().then(() => {
        redirect({}, '/signup');
      });
    }
  };

  const responseGoogleOnFailure = async response => {
    console.log(response);
  };

  return (
    <GoogleLogin
      disabledStyle
      clientId="397941065547-2p71r7m2jneh4a22hckkfvnqsfivk4g0.apps.googleusercontent.com"
      buttonText="Login"
      onSuccess={responseGoogleOnSuccess}
      onFailure={responseGoogleOnFailure}
      cookiePolicy={'single_host_origin'}
      render={renderProps => (
        <div onClick={renderProps.onClick}>
          <ButtonGoogle />
        </div>
      )}
    />
  );
};

export { FacebookLoginForm, GoogleLoginForm };
