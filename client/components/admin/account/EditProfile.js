import React, { useState, useCallback } from 'react';
import gql from 'graphql-tag';
import { useMutation, useQuery } from 'react-apollo';
import Cropper from 'react-easy-crop';
import styled from 'styled-components';
import getCroppedImg from '../../../lib/cropImage';
import { CURRENT_USER_QUERY } from '../../../lib/checkLoggedIn';
import useToggle from '../../hooks/useToggle';
import EditProfileForm from '../../common/admin/form/EditProfileForm';
import { Text } from '../../styles/Text';
import { Card, CardBody } from '../../styles/StyledCard';

const Wrapper = styled.div`
  min-height: 100vh;
  background-color: #fafafa;
  padding: 24px 32px;

  .modal {
    background-color: rgba(0, 0, 0, 0.4);

    &-body {
      height: 250px;
    }
  }

  [type='file'] {
    height: 0;
    overflow: hidden;
    width: 0;
  }
  [type='file'] + label {
    border-style: none;
    border-radius: 6px;
    border: solid 1px #ff438c;
    background-color: #ffffff;
    color: #ff438c;
    cursor: pointer;
    font-family: 'Open Sans', sans-sherif;
    font-weight: 600;
    text-align: center;
    margin-bottom: 1rem;
    outline: none;
    line-height: 1.25;
    padding: 9px 12px;
    font-size: 14px;
    position: relative;
    transition: all 0.3s;
    vertical-align: middle;

    &:hover {
      background-color: #ff438c;
      color: #ffffff;
    }
  }
`;

const UPLOAD_PICTURE_MUTATION = gql`
  mutation UPLOAD_PICTURE_MUTATION($picture: String!) {
    uploadFile(picture: $picture) {
      errors {
        value
        param
      }
    }
  }
`;

const EditProfile = () => {
  const onCompleted = data => {
    if (!data.uploadFile.errors) {
      setSuccess('Sudah terupdate');
    }
  };

  const onError = error => {
    // If you want to send error to external service?
    console.error(error);
  };

  const { data, errorUser, loading } = useQuery(CURRENT_USER_QUERY);
  const [success, setSuccess] = useState();
  const [imageCrop, setImageCrop] = useState();
  const [fileKey, setFileKey] = useState(Date.now());
  const [showModal, handleModal] = useToggle();
  const [croppedAreaPixels, setCropArea] = useState();
  const [crop, setCrop] = useState({ x: 0, y: 0 });
  const [zoom, setZoom] = useState(1);
  const onCropComplete = useCallback((croppedArea, croppedAreaPixels) => {
    setCropArea(croppedAreaPixels);
  }, []);
  const [uploadPictureMutation, { error }] = useMutation(UPLOAD_PICTURE_MUTATION, { onCompleted, onError });

  const uploadPicture = async croppedImage => {
    await uploadPictureMutation({
      variables: { picture: croppedImage },
      refetchQueries: [{ query: CURRENT_USER_QUERY }]
    });
  };

  const showCroppedImage = async () => {
    const croppedImage = await getCroppedImg(imageCrop, croppedAreaPixels);
    uploadPicture(croppedImage);
    closeModal();
  };

  const handleFile = e => {
    e.preventDefault();

    let reader = new FileReader();
    let file = e.target.files[0];

    if (file) {
      if (file.size > 2097152) {
        alert('File size exceeds 2 MB');
      } else {
        reader.onloadend = () => {
          setImageCrop(reader.result);
        };
        reader.readAsDataURL(file);

        handleModal();
      }
    }
  };

  const closeModal = () => {
    setFileKey(Date.now());
    handleModal();
  };

  return (
    <Wrapper>
      <Modal
        imageCrop={imageCrop}
        showModal={showModal}
        closeModal={closeModal}
        crop={crop}
        zoom={zoom}
        setCrop={setCrop}
        setZoom={setZoom}
        onCropComplete={onCropComplete}
        showCroppedImage={showCroppedImage}
      />
      <Card>
        {!loading && (
          <CardBody>
            <Text xl color="#092041" weight="800" className="mb-4 text-center text-md-left">
              Edit Profile
            </Text>
            <div className="d-flex flex-column bd-highlight mb-3">
              <div className="d-flex flex-md-row flex-column flex-wrap mb-4 align-items-center align-items-md-start">
                {!data.me.profile.picture ? (
                  <img
                    src={'http://safetravel.id/img/default.png'}
                    alt="Profile Image"
                    className="card-body-image rounded-circle"
                  />
                ) : (
                  <img
                    src={`http://localhost:5000/img/${data.me.profile.picture}`}
                    alt="Profile Image"
                    className="card-body-image rounded-circle"
                  />
                )}

                <div className="pt-3 pl-md-4 text-center text-md-left">
                  <Text lg weight="800" color="#092041">
                    {data.me.profile.fullname}
                  </Text>
                  <input type="file" id="file" name="file" className="mb-3" key={fileKey} onChange={handleFile} />
                  <label htmlFor="file">Unggah Foto</label>
                  <Text sm color="#092041" opacity="0.6" className="mb-0">
                    Ukuran gambar 256 x 256px PNG atau JPG.
                  </Text>
                  <span className="text-success"> {success}</span>
                </div>
              </div>
              <EditProfileForm dataProfile={data} />
            </div>
          </CardBody>
        )}
      </Card>
    </Wrapper>
  );
};

const Modal = ({
  imageCrop,
  showModal,
  closeModal,
  setCrop,
  setZoom,
  onCropComplete,
  crop,
  zoom,
  showCroppedImage
}) => (
  <div
    className={`modal fade ${showModal ? 'show d-block' : ''}`}
    id="exampleModalLong"
    tabIndex="-1"
    role="dialog"
    aria-labelledby="exampleModalLongTitle"
    aria-hidden="true">
    <div className="modal-dialog modal-dialog-centered" role="document">
      <div className="modal-content">
        <div className="modal-header">
          <h5 className="modal-title" id="exampleModalLongTitle">
            Position and size your new avatar
          </h5>
          <button type="button" className="close" data-dismiss="modal" aria-label="Close" onClick={closeModal}>
            <span aria-hidden="true">&times;</span>
          </button>
        </div>
        <div className="modal-body">
          {imageCrop && (
            <Cropper
              image={imageCrop}
              crop={crop}
              zoom={zoom}
              aspect={3 / 3}
              onCropChange={setCrop}
              onCropComplete={onCropComplete}
              onZoomChange={setZoom}
            />
          )}
        </div>
        <div className="modal-footer">
          <button onClick={showCroppedImage} className="btn btn-primary">
            Set as profile
          </button>
        </div>
      </div>
    </div>
  </div>
);

export default EditProfile;
