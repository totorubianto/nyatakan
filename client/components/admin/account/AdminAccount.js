import React from 'react';
import styled from 'styled-components';
import gql from 'graphql-tag';
import Table from '../../common/admin/Table';
import AccountTableContent from '../../common/admin/Table/AccountTableContent';
import StoreTableContent from '../../common/admin/Table/StoreTableContent';
import VerifTableContent from '../../common/admin/Table/VerifTableContent';
import {  TabPanel } from 'react-tabs';
import { useQuery } from 'react-apollo';
import { Text, Heading } from '../../styles/Text';
import { Card, CardBody } from '../../styles/StyledCard'
import { StyledTabs, StyledTab, StyledTabList } from '../../styles/StyledReactTabs';

const Wrapper = styled.div`
  min-height: 100vh;
  background-color: #fafafa;
  padding: 24px 32px;

  .table-content {
    margin-top: 24px;
  }

  .overlay {
    position: absolute;
    z-index: 1;
    top: 0;
    left 0;
    height: 100vh;
    background: #333;
    opacity: 0.5
  }

  .row {
    margin: 0px;
  }
`;

const STORE_QUERY = gql`
  query {
    getStores {
      _id cover logo store_name username_store username_youtube
      username_twitter username_instagram ishidden
      profile{
        fullname
        saldo
      }
    }

    getAllUsers {
      fullname dateofbirth job phone address city picture
      region state phone phone userclass
      user {
        username email
      }
      
      stores {
        store_name
        username_youtube
        username_store
        username_twitter
        username_instagram
      }
    }
  }
`;

const AdminAccount = () => {

  const{ loading, error, data } = useQuery(STORE_QUERY);
  return (
    <Wrapper>
      <div className="card-deck col-hidden">
        <Card className="card">
          <CardBody>
            <Text lg color="#bdbdbd" weight="600">
              Jumlah Akun
            </Text>
            <Heading h2 color="#092041" bold>
              20 Akun
            </Heading>
            <Text md color="#bdbdbd" weight="600" className="mt-3">
              Terdapat 10 akun creator
            </Text>
          </CardBody>
        </Card>
        <Card className="card">
          <CardBody>
            <Text lg color="#bdbdbd" weight="600">
              Jumlah Store
            </Text>
            <Heading h2 color="#092041" bold>
              {loading === false ? data.getStores.length : "-"} Request
            </Heading>
          </CardBody>
        </Card>
        <Card className="card">
          <CardBody>
            <Text lg color="#bdbdbd" weight="600">
              Verifikasi Akun
            </Text>
            <Heading h2 color="#092041" bold>
              10 Request
            </Heading>
          </CardBody>
        </Card>
      </div>

      <StyledTabs
        selectedTabClassName="active"
      >
        <StyledTabList>
          <StyledTab><a>Akun</a></StyledTab>
          <StyledTab><a>Store</a></StyledTab>
          <StyledTab><a>Verifikasi Kreator</a></StyledTab>
        </StyledTabList>

        <div className="table-content">
          <Card>
            <CardBody>
              <TabPanel>
                <Text lg color="#092041" weight="800">Akun</Text>
                <Table>
                  {loading === false && <AccountTableContent content={data.getAllUsers}/>}
                </Table>
              </TabPanel>

              <TabPanel>
                <Text lg color="#092041" weight="800">Store</Text>
                <Table>
                  {loading === false && <StoreTableContent content={data.getStores} />}
                </Table>
              </TabPanel>

              <TabPanel>
                <Text lg color="#092041" weight="800">Verifikasi Kreator</Text>
                <Table>
                  {loading === false && <VerifTableContent />}
                </Table>
              </TabPanel>
            </CardBody>
          </Card>
        </div>
      </StyledTabs>
    </Wrapper>
  );
};

export default AdminAccount;
