import { useState, useEffect } from 'react';

const useForm = (callback, validate, date) => {
  const [values, setValues] = useState({});
  const [errors, setErrors] = useState({});
  const [isSubmitting, setIsSubmitting] = useState(false);

  useEffect(() => {
    if (Object.keys(errors).length === 0 && isSubmitting) {
      callback();
    }
  }, [errors]);

  const handleSubmit = e => {
    if (e) e.preventDefault();
    if (Object.keys(values).length === 0) return;
    setIsSubmitting(true);
    setErrors(validate(values, date));
  };

  const handleChange = e => {
    e.persist();
    if (e.target.type === 'checkbox') {
      setValues(values => ({ ...values, [event.target.name]: event.target.checked }));
    } else {
      setValues(values => ({ ...values, [event.target.name]: event.target.value }));
    }
  };

  return {
    handleChange,
    handleSubmit,
    values,
    errors
  };
};

export default useForm;
