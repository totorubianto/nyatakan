import validator from 'validator';

export function ValidateSignup(values) {
  let errors = {};
  if (!values.email) {
    errors.email = 'Please include a valid email...';
  }
  if (!values.email || !validator.isEmail(values.email)) {
    errors.email = 'Please include a valid email...';
  }
  if (!values.username) {
    errors.username = 'Username is required...';
  }
  if (!values.password) {
    errors.password = 'Please enter a password with 6 or more characters...';
  }
  if (!values.password || !validator.isLength(values.password, { min: 6 })) {
    errors.password = 'Please enter a password with 6 or more characters...';
  }
  return errors;
}

export function ValidateSigin(values) {
  let errors = {};
  if (!values.email) {
    errors.email = 'Please include a valid email or username...';
  }
  if (!values.password) {
    errors.password = 'Please enter a password with 6 or more characters...';
  }
  if (!values.password || !validator.isLength(values.password, { min: 6 })) {
    errors.password = 'Please enter a password with 6 or more characters...';
  }
  return errors;
}

export function ValidateForgetPass(values) {
  let errors = {};
  if (!values.email || !validator.isEmail(values.email)) {
    errors.email = 'Please include a valid email...';
  }
  return errors;
}

export function ValidateAddprofile(values, date) {
  let errors = {};
  if (!values.userclass) {
    errors.userclass = 'User as is required...';
  }
  if (!values.fullname) {
    errors.fullname = 'Full name is required...';
  }
  if (!date) {
    errors.dateofbirth = 'Date of Birth name is required...';
  }
  if (!values.phone) {
    errors.phone = 'Phone is required...';
  }
  if (!values.phone || !validator.isNumeric(values.phone)) {
    errors.phone = 'Phone must be a number...';
  }
  if (!values.phone || !validator.isLength(values.phone, { min: 9, max: 15 })) {
    errors.phone = 'Phone number must be at least 9 characters...';
  }
  if (!values.job) {
    errors.job = 'Job is required...';
  }
  return errors;
}

export function ValidateUpdateprofile(values) {
  let errors = {};

  if (values.email && !validator.isEmail(values.email)) {
    errors.email = 'Please include a valid email...';
  }

  if (values.phone && !validator.isNumeric(values.phone)) {
    errors.phone = 'Phone must be a number...';
  }

  if (values.phone && !validator.isLength(values.phone, { min: 9, max: 15 })) {
    errors.phone = 'Phone number must be at least 9 characters...';
  }

  return errors;
}

export function ValidateAddstore(values, date) {
  let errors = {};
  if (!values.store_name) {
    errors.store_name = 'Name Store is required...';
  }
  if (!values.username_store) {
    errors.username_store = 'Store Username is required...';
  }
  if (!values.username_instagram) {
    errors.username_instagram = 'Instagram Username is required...';
  }
  if (!values.username_youtube) {
    errors.username_youtube = 'Youtube Username is required...';
  }
  if (!values.username_twitter) {
    errors.username_twitter = 'Twitter Username is required...';
  }
  if (values.agree === false || values.agree === undefined) {
    errors.agree = '..';
  }
  return errors;
}
