import { useState, useEffect } from 'react';

const useUpdateForm = (callback, validate) => {
  const [values, setValues] = useState(data);
  const [errors, setErrors] = useState({});
  const [isSubmitting, setIsSubmitting] = useState(false);

  useEffect(() => {
    if (Object.keys(errors).length === 0 && isSubmitting) {
      callback();
    }
  }, [errors]);

  const handleSubmit = e => {
    if (e) e.preventDefault();
    setIsSubmitting(true);
    setErrors(validate(values));
  };

  const handleChange = e => {
    e.persist();
    if (e.target.type === 'checkbox') {
      setValues(values => ({ ...values, [event.target.name]: event.target.checked }));
    } else {
      setValues(values => ({ ...values, [event.target.name]: event.target.value }));
    }
  };

  return {
    handleChange,
    handleSubmit,
    values,
    errors
  };
};

export default useUpdateForm;
