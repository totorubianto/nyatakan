import React, { useState, useEffect } from 'react';
import styled from 'styled-components';
import ForgotPassForm from './ForgotPassForm';
import { Heading, Text } from '../styles/Text';

const Wrapper = styled.div`
  width: 510px;
  border-radius: 20px;
  background-color: #ffffff;
  margin-top: 60px;
  margin-bottom: calc(100vh - 70vh);

  .forgotpass {
    &-header {
      padding: 32px 32px 16px;
    }

    &-wrapper {
      padding: 0 32px;
    }

    &-arrow {
      padding-right: 16px;
      &:hover {
        cursor: pointer;
      }
    }
  }

  .daftar-link {
    font-family: 'Open Sans', sans-serif;
    font-size: 14px;
    font-weight: normal;
    font-style: normal;
    font-stretch: normal;
    line-height: 1.29;
    letter-spacing: normal;
    color: #4380ff !important;

    &:hover {
      cursor: pointer;
      color: #4380ff !important;
    }
  }

  @media (max-width: 768px) {
    width: auto;
    margin-top: 40px;
    margin-bottom: 20px;
  }

  @media (max-width: 576px) {
    width: auto;
    margin-right: 1%;
    margin-left: 1%;
  }
`;

function ForgotPassWrapper() {
  const [email, setEmail] = useState(null);

  const updateErrors = data => {
    setEmail(data);
  };

  return (
    <Wrapper className="container">
      <div className="forgotpass-header d-flex">
        <img src="/static/icons/icons_arrow_left.svg" className="forgotpass-arrow" alt="Back" />
        <Heading h3 color="black mb-0" bold>
          Lupa Password
        </Heading>
      </div>

      <div className="forgotpass-wrapper pb-4">
        {email !== null ? (
          <Text lg color="#092041" opacity="0.6">
            Password anda telah diganti. Silahkan buka email anda
          </Text>
        ) : (
          <React.Fragment>
            <Text lg color="#092041" opacity="0.6">
              Masukkan alamat email, kami akan mengirimkan verifikasi untuk mengembalikan password.
            </Text>
            <ForgotPassForm setEmail={updateErrors} />
          </React.Fragment>
        )}
      </div>
    </Wrapper>
  );
}

export default ForgotPassWrapper;
