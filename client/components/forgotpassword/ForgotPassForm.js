import React, { useState } from 'react'
import gql from 'graphql-tag';
import useForm from '../hooks/useForm';
import TextFieldGroup from '../common/TextFieldGroup';
import { ValidateForgetPass } from '../hooks/useInputValidate';
import { useMutation } from 'react-apollo';

const FORGET_PASS_MUTATION = gql`
    mutation CHANGE_PASS_MUTATION($email: String!) {
        forgetPass(email: $email) {
            email
            errors {
                value
                param
            }
        }
    }
`

export default function ForgotPassForm({ setEmail }) {
    const {handleChange, handleSubmit, values, errors} = useForm(forgetPassword, ValidateForgetPass);
    const [forgetPassMutation, {data}] = useMutation(FORGET_PASS_MUTATION, {
        variables: {email: values.email}
    })

    async function forgetPassword() {
        const res = await forgetPassMutation();
        setEmail(res.data.forgetPass.email);
    }

    return (
        <form onSubmit={handleSubmit}>
            <TextFieldGroup 
                name="email"
                placeholder="Masukkan Email yang Anda Daftarkan"
                label="Email" 
                value={values.email || ''}
                onChange={handleChange}
                error={
                    errors.email ||
                    (data &&
                     data.forgetPass.errors.length > 0 &&
                     data.forgetPass.errors[0].param === 'info' &&
                     data.forgetPass.errors[0].value)
                }/>
            <button
                type="submit"
                className="btn btn-primary btn-md btn-block border-0 mb-2 p-2"
                style={{ backgroundColor: '#ff438c' }}>
                Konfirmasi
            </button>
        </form>
    )
}


