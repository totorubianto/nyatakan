import React, { useReducer } from 'react';
import Link from 'next/link';
import { Wrapper } from './styles/SignupWrapperStyles';
import { ButtonFacebook, ButtonGoogle } from '../common/CostumButton';
import SignupForm from './components/SignupForm';
import SignupForm2 from './components/SignupForm2';
import SignupForm3 from './components/SignupForm3';
import { Heading, Text } from '../styles/Text';

function SignupWrapper({ step }) {
  const initialState = {
    step: step || 0
  };

  const UPDATE_STEP = 'UPDATE_STEP';

  function reducer(state, action) {
    switch (action.type) {
      case UPDATE_STEP:
        return {
          step: state.step + 1
        };
      default:
        return initialState;
    }
  }

  const [state, dispatch] = useReducer(reducer, initialState);
  const Header = ['Daftar', 'Identitas', 'Jenis Akun'];

  return (
    <Wrapper className="container">
      <div className="register-header">
        <div className="mb-3">
          <img src="/static/icons/icons_arrow_left.svg" className="register-arrow" alt="Back" />
          <Heading h3 color="black ">
            {Header[state.step]}
          </Heading>
        </div>

        {state.step !== 0 && (
          <ul className="register-progress d-flex justify-content-between pl-0 mb-0">
            <li className={state.step !== 0 ? 'active' : ''} />
            <li className={state.step === 2 ? 'active' : ''} />
            <li />
          </ul>
        )}
      </div>
      {state.step === 0 && <SignupStep1 dispatch={dispatch} />}
      {state.step === 1 && <SignupStep2 dispatch={dispatch} />}
      {state.step === 2 && <SignupStep3 />}
    </Wrapper>
  );
}

const SignupStep1 = ({ dispatch }) => {
  return (
    <div className="register-wrapper pb-4">
      <SignupForm dispatch={dispatch} />

      <div className="d-flex align-items-center mb-2">
        <hr className="w-100" />
        <Text sm color="#092041" className="mx-3">
          atau
        </Text>
        <hr className="w-100" />
      </div>

      <ButtonGoogle></ButtonGoogle>
      <ButtonFacebook></ButtonFacebook>

      <div className="text-center">
        <span>
          Sudah punya akun?{' '}
          <Link href="/login">
            <a className="daftar-link">Login</a>
          </Link>
        </span>
      </div>
    </div>
  );
};

const SignupStep2 = ({ dispatch }) => {
  return (
    <div className="register-wrapper pb-4">
      <SignupForm2 dispatchStep={dispatch} />
    </div>
  );
};

const SignupStep3 = () => {
  return (
    <div className="register-wrapper pb-4">
      <SignupForm3 />
    </div>
  );
};
export default SignupWrapper;
