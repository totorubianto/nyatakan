import styled, { keyframes } from 'styled-components';

const StepbarAnimationIn = keyframes`
  0% {
      width: 0;
    }
  100% {
    width: 135px;
  }
`;

export const Wrapper = styled.div`
  width: 510px;
  border-radius: 20px;
  background-color: #ffffff;
  margin-top: 60px;
  margin-bottom: 40px;

  .register {
    &-header {
      padding: 32px 32px 16px;
    }

    &-wrapper {
      padding: 0 32px;
    }

    &-progress {
      width: 100%;
      counter-reset: step;

      li {
        list-style-type: none;
        position: relative;

        &::before {
          content: counter(step);
          counter-increment: step;
          z-index: 2;
          display: block;
          width: 44px;
          height: 44px;
          margin: 0 0 10px 0;
          font-size: 18px;
          font-family: 'OpenSans', sans-serif;
          font-weight: bold;
          line-height: 41px;
          text-align: center;
          border: solid 2px rgba(67, 128, 255, 0.4);
          border-radius: 50%;
          background-color: #ffffff;
          color: #092041;
        }

        &::after {
          content: '';
          position: absolute;
          width: 135px;
          height: 1px;
          background-color: rgba(67, 128, 255, 0.4);
          top: 22px;
          left: 107%;
          z-index: 1;
        }

        &:last-child:after {
          content: none;
        }
        &.active {
          &:after {
            background-color: rgba(67, 128, 255, 0.4);
          }

          &:first-child:after {
            background-color: #4380ff;
            animation: ${StepbarAnimationIn} 1s ease;
          }

          &:nth-child(2):after {
            background-color: #4380ff;
            animation: ${StepbarAnimationIn} 1s ease;
            animation-delay: 0.3s;
          }

          &:before {
            content: url('/static/icons/icon_approve.svg');
            background-color: #4380ff;
            color: #ffffff;
          }

          & + li:before {
            background-color: #4380ff;
            color: #ffffff;
          }
        }
      }
    }

    &-arrow {
      padding-right: 16px;
      &:hover {
        cursor: pointer;
      }
    }
  }

  .recaptcha {
    margin: auto;
  }

  .line {
    width: 161px;
    height: 2px;
    background-color: #e2e2e2;
  }

  .daftar-link {
    font-weight: normal;
    font-style: normal;
    font-stretch: normal;
    line-height: 1.29;
    letter-spacing: normal;
    color: #4380ff !important;

    &:hover {
      cursor: pointer;
      color: #4380ff !important;
    }
  }

  .description {
    width: 80%;
  }

  @media screen and (max-width: 767px) {
    width: auto;
    margin-top: 40px;
    margin-bottom: 20px;

    .register-progress li::after {
      width: 150px;
    }
  }

  @media screen and (max-width: 575.5px) {
    width: auto;
    margin-right: 1%;
    margin-left: 1%;

    .register-progress li::after {
      width: 161px;
    }
  }

  @media screen and (max-width: 566.5px) {
    .register-progress li::after {
      width: 157px;
    }
  }

  @media screen and (max-width: 426px) {
    .register-progress li::after {
      width: 90px;
    }
  }

  @media screen and (max-width: 376px) {
    .register-progress li::after {
      width: 64px;
    }
  }
`;
