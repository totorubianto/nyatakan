import React from 'react';
import gql from 'graphql-tag';
import { useMutation, useApolloClient } from '@apollo/react-hooks';
import useForm from '../../hooks/useForm';
import redirect from '../../../lib/redirect';
import { ValidateAddstore } from '../../hooks/useInputValidate';
import TextFieldGroup from '../../common/TextFieldGroup';

import CheckboxInput from '../../common/CheckboxInput';
import { Text } from '../../styles/Text';

const ADDSTORE_MUTATION = gql`
  mutation ADDSTORE_MUTATION(
    $store_name: String!
    $username_store: String!
    $username_instagram: String!
    $username_youtube: String!
    $username_twitter: String!
  ) {
    addStore(
      store: {
        store_name: $store_name
        username_store: $username_store
        username_instagram: $username_instagram
        username_youtube: $username_youtube
        username_twitter: $username_twitter
      }
    ) {
      errors {
        value
        param
      }
    }
  }
`;

export default function SignupForm3() {
  const client = useApolloClient();

  const onCompleted = data => {
    if (!data.addStore.errors.length > 0) {
      // logged in
      client.cache.reset().then(() => {
        redirect({}, '/');
      });
    }
  };
  const onError = error => {
    // If you want to send error to external service?
    console.error(error);
  };

  const { values, handleChange, errors, handleSubmit } = useForm(addStore, ValidateAddstore);
  const [addStoreMutation, { data, error, loading }] = useMutation(ADDSTORE_MUTATION, { onCompleted, onError });

  async function addStore() {
    await addStoreMutation({
      variables: {
        store_name: values.store_name,
        username_store: values.username_store,
        username_instagram: values.username_instagram,
        username_youtube: values.username_youtube,
        username_twitter: values.username_twitter
      }
    });
  }

  return (
    <form onSubmit={handleSubmit}>
      <TextFieldGroup
        name="store_name"
        placeholder="Nama Store"
        label="Nama Store"
        value={values.store_name || ''}
        onChange={handleChange}
        error={
          errors.store_name ||
          (data &&
            data.addStore.errors.length > 0 &&
            data.addStore.errors[0].param === 'store_name' &&
            data.addStore.errors[0].value)
        }
      />
      <TextFieldGroup
        name="username_store"
        placeholder="Username Store"
        label="Username Store"
        value={values.username_store || ''}
        onChange={handleChange}
        error={
          errors.username_store ||
          (data &&
            data.addStore.errors.length > 0 &&
            data.addStore.errors[0].param === 'username_store' &&
            data.addStore.errors[0].value)
        }
      />
      <TextFieldGroup
        name="username_instagram"
        placeholder="Username Instagram"
        label="Username Instagram"
        value={values.username_instagram || ''}
        onChange={handleChange}
        error={
          errors.username_instagram ||
          (data &&
            data.addStore.errors.length > 0 &&
            data.addStore.errors[0].param === 'username_instagram' &&
            data.addStore.errors[0].value)
        }
      />
      <TextFieldGroup
        name="username_youtube"
        placeholder="Channel Youtube"
        label="Channel Youtube"
        value={values.username_youtube || ''}
        onChange={handleChange}
        error={
          errors.username_youtube ||
          (data &&
            data.addStore.errors.length > 0 &&
            data.addStore.errors[0].param === 'username_youtube' &&
            data.addStore.errors[0].value)
        }
      />
      <TextFieldGroup
        name="username_twitter"
        placeholder="Username Twitter"
        label="Username Twitter"
        value={values.username_twitter || ''}
        onChange={handleChange}
        error={
          errors.username_twitter ||
          (data &&
            data.addStore.errors.length > 0 &&
            data.addStore.errors[0].param === 'username_twitter' &&
            data.addStore.errors[0].value)
        }
      />
      <div className="description mb-1">
        <Text sm>Username ini akan digunakan untuk verifikasi akun Nyatakan Kreator</Text>
      </div>
      <CheckboxInput
        name="agree"
        type="checkbox"
        label="Dengan ini saya setuju terhadap ketentuan pengguna"
        className="mb-3"
        checked={values.agree}
        onChange={handleChange}
        error={errors.agree}
      />
      <button
        type="submit"
        className="btn btn-primary btn-md btn-block border-0 mb-2 p-2"
        style={{ backgroundColor: '#ff438c' }}>
        Daftar
      </button>
    </form>
  );
}
