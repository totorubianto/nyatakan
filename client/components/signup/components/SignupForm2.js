import React, { useReducer } from 'react';
import { useMutation, useApolloClient } from '@apollo/react-hooks';
import gql from 'graphql-tag';
import useForm from '../../hooks/useForm';
import { ValidateAddprofile } from '../../hooks/useInputValidate';
import TextFieldGroup from '../../common/TextFieldGroup';
import SelectInput from '../../common/SelectInput';
import DatePickerInput from '../../common/DatePickerGroup';

const initialState = {
  dateofbirth: undefined,
  isEmpty: true,
  isDisabled: false
};

function reducer(state, action) {
  switch (action.type) {
    case 'dateChange':
      return action.payload;
    default:
      throw new Error();
  }
}

const ADDPROFILE_MUTATION = gql`
  mutation ADDPROFILE_MUTATION(
    $fullname: String!
    $dateofbirth: String!
    $phone: String!
    $job: String!
    $userclass: String!
  ) {
    addProfile(
      profile: {
        fullname: $fullname
        dateofbirth: $dateofbirth
        phone: $phone
        job: $job
        userclass: $userclass
      }
    ) {
      errors {
        value
        param
      }
    }
  }
`;

export default function SignupForm2({ dispatchStep }) {
  // Select options for proffesion
  const jobOptions = [
    { label: '', value: '', disabled: true },
    { label: 'Yotuber', value: 'Yotuber' },
    { label: 'Selebgram', value: 'Selebgram' },
    { label: 'Selebtweet', value: 'Selebtweet' }
  ];

  // Select options for user class
  const userOptions = [
    { label: '', value: '', disabled: true },
    { label: 'Normal', value: 'normal' },
    { label: 'Seller', value: 'seller' },
    { label: 'Admin', value: 'admin' }
  ];

  const onCompleted = data => {
    if (data.addProfile.errors === null) {
      dispatchStep({ type: 'UPDATE_STEP' });
    }
  };
  const onError = error => {
    // If you want to send error to external service?
    console.error(error);
  };

  const [state, dispatch] = useReducer(reducer, initialState);
  const { values, handleChange, errors, handleSubmit } = useForm(
    addprofile,
    ValidateAddprofile,
    state.dateofbirth
  );
  const [addprofileMutation, { data, error, loading }] = useMutation(
    ADDPROFILE_MUTATION,
    { onCompleted, onError }
  );

  async function addprofile() {
    await addprofileMutation({
      variables: {
        fullname: values.fullname,
        dateofbirth: state.dateofbirth,
        phone: values.phone,
        job: values.job,
        userclass: values.userclass
      }
    });
  }

  function handleDayChange(selectedDay, modifiers, dayPickerInput) {
    const input = dayPickerInput.getInput();
    dispatch({
      type: 'dateChange',
      payload: {
        dateofbirth: selectedDay,
        isEmpty: !input.value.trim(),
        isDisabled: modifiers.disabled === true
      }
    });
  }

  return (
    <form onSubmit={handleSubmit}>
      <SelectInput
        name='userclass'
        placeholder='Akun Biasa'
        label='Daftar Sebagai'
        value={values.userclass || ''}
        options={userOptions}
        onChange={handleChange}
        error={
          errors.userclass ||
          (data &&
            data.addProfile.errors &&
            data.addProfile.errors[0].param === 'userclass' &&
            data.addProfile.errors[0].value)
        }
      />
      <TextFieldGroup
        name='fullname'
        placeholder='Masukkan Nama Lengkap'
        label='Nama Lengkap'
        value={values.fullname || ''}
        onChange={handleChange}
        error={
          errors.fullname ||
          (data &&
            data.addProfile.errors &&
            data.addProfile.errors[0].param === 'fullname' &&
            data.addProfile.errors[0].value)
        }
      />
      <DatePickerInput
        name='dateofbirth'
        label='Tanggal Lahir'
        value={state.dateofbirth}
        onChange={handleDayChange}
        error={
          errors.dateofbirth ||
          (data &&
            data.addProfile.errors &&
            data.addProfile.errors[0].param === 'dateofbirth' &&
            data.addProfile.errors[0].value)
        }
      />
      <TextFieldGroup
        name='phone'
        placeholder='Isi Dengan Angka'
        label='Nomor HP'
        value={values.phone || ''}
        onChange={handleChange}
        error={
          errors.phone ||
          (data &&
            data.addProfile.errors &&
            data.addProfile.errors[0].param === 'phone' &&
            data.addProfile.errors[0].value)
        }
      />
      <SelectInput
        name='job'
        placeholder='Youtuber'
        label='Saya Adalah'
        value={values.job || ''}
        options={jobOptions}
        onChange={handleChange}
        error={
          errors.job ||
          (data &&
            data.addProfile.errors &&
            data.addProfile.errors[0].param === 'job' &&
            data.addProfile.errors[0].value)
        }
      />
      <button
        type='submit'
        className='btn btn-primary btn-md btn-block border-0 mb-2 p-2'
        style={{ backgroundColor: '#ff438c' }}
      >
        Selanjutnya
      </button>
    </form>
  );
}
