import React from 'react';
import { useMutation } from '@apollo/react-hooks';
import gql from 'graphql-tag';
import cookie from 'cookie';
import useToggle from '../../hooks/useToggle';
import useForm from '../../hooks/useForm';
import { ValidateSignup } from '../../hooks/useInputValidate';
import TextFieldGroup from '../../common/TextFieldGroup';
import PasswordTextField from '../../common/PasswordTextField';

const SIGNUP_MUTATION = gql`
  mutation SIGNUP_MUTATION($email: String!, $username: String!, $password: String!) {
    signUpUser(user: { email: $email, username: $username, password: $password }) {
      token
      errors {
        value
        param
      }
    }
  }
`;

export default function SignupForm({ dispatch }) {
  const onCompleted = data => {
    if (!data.signUpUser.errors.length > 0) {
      // Store the token in cookie
      document.cookie = cookie.serialize('token', data.signUpUser.token, {
        maxAge: 30 * 24 * 60 * 60, // 30 days
        path: '/' // make cookie available for all routes underneath "/"
      });

      dispatch({ type: 'UPDATE_STEP' });
    }
  };
  const onError = error => {
    // If you want to send error to external service?
    console.error(error);
  };

  const [visible, toggleIsVisible] = useToggle(false);
  const { values, handleChange, errors, handleSubmit } = useForm(signup, ValidateSignup);
  const [signupMutation, { data, loading }] = useMutation(SIGNUP_MUTATION, { onCompleted, onError });

  async function signup() {
    await signupMutation({
      variables: { email: values.email, username: values.username, password: values.password }
    });
  }

  return (
    <form onSubmit={handleSubmit}>
      <TextFieldGroup
        name="email"
        placeholder="Masukkan Email"
        label="Email"
        value={values.email || ''}
        onChange={handleChange}
        error={
          errors.email ||
          (data &&
            data.signUpUser.errors.length > 0 &&
            data.signUpUser.errors[0].param === 'email' &&
            data.signUpUser.errors[0].value)
        }
      />
      <TextFieldGroup
        name="username"
        placeholder="Masukkan Username"
        label="Username"
        value={values.username || ''}
        onChange={handleChange}
        error={
          errors.username ||
          (data &&
            data.signUpUser.errors.length > 0 &&
            data.signUpUser.errors[0].param === 'username' &&
            data.signUpUser.errors[0].value)
        }
      />
      <PasswordTextField
        type={visible ? 'text' : 'password'}
        name="password"
        placeholder="Masukkan Password"
        label="Password"
        value={values.password || ''}
        onChange={handleChange}
        toggleVisible={toggleIsVisible}
        forgetPassword={false}
        error={
          errors.password ||
          (data &&
            data.signUpUser.errors.length > 0 &&
            data.signUpUser.errors[0].param === 'password' &&
            data.signUpUser.errors[0].value)
        }
      />

      <button
        type="submit"
        className="btn btn-primary btn-md btn-block border-0 mb-2 p-2"
        style={{ backgroundColor: '#ff438c' }}>
        Daftar
      </button>
    </form>
  );
}
