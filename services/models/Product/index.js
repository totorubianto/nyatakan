import mongoose from "mongoose";
import { ObjectID } from "mongodb";

const Schema = mongoose.Schema;

ObjectID.prototype.valueOf = function() {
  return this.toString();
};

const ProductSchema = new Schema({
  slug: {
    type: String,
    required: true
  },
  productname: {
    type: String,
    required: true
  },
  description: {
    type: String
  },
  cost: {
    type: Number,
    default:0,
    required: true
  },
  stock: {
    type: Number,
    default:0
  },
  productstatus: {
    type: String,
    required: true
  },
  ishidden: {
    type: Boolean,
    required: true,
    default:false
  },
  baseproduct: {
    type: Schema.Types.ObjectId,
    ref: "baseproducts"
  }
});

export default mongoose.model("Product", ProductSchema);
