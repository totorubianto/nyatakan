const mongoose = require("mongoose");

const Schema = mongoose.Schema;

const StoreSchema = new Schema({
  profile: {
    type: mongoose.Schema.Types.ObjectId,
    ref: "profile"
  },
  cover: {
    type: String
  },
  logo: {
    type: String
  },
  store_name: {
    type: String,
    required: true
  },
  username_instagram: {
    type: String,
    required: true
  },
  username_youtube: {
    type: String,
    required: true
  },
  username_twitter: {
    type: String,
    required: true
  },
  username_store: {
    type: String,
    required: true,
    unique: true
  },
  ishidden: {
    type: Boolean,
    required: true,
    default: false
  },
  collections: [
      {
      type: Schema.Types.ObjectId,
      ref: "collections"
    }
  ]
});

module.exports = Store = mongoose.model("store", StoreSchema);
