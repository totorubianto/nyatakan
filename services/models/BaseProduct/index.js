import mongoose from "mongoose";
import { ObjectID } from "mongodb";

const Schema = mongoose.Schema;

ObjectID.prototype.valueOf = function() {
  return this.toString();
};

const BaseProductSchema = new Schema({
  baseproduct_name: {
    type: String,
    required: true
  },
  category: {
    type: String,
    required: true
  },
  description: {
    type: String
  },
  color: {
    type: [String],
    required: true
  },
  size: {
    type: [String],
    required: true
  },
  cost: {
    type: Number,
    required: true,
    default:0
  },
  material: {
    type: Schema.Types.ObjectId,
    ref: "materials"
  }
});

export default mongoose.model("BaseProduct", BaseProductSchema);
