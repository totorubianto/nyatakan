const mongoose = require('mongoose');

const Schema = mongoose.Schema;

const ProfileSchema = new Schema({
  user: {
    type: mongoose.Schema.Types.ObjectId,
    ref: 'user'
  },
  picture: {
    type: String
  },
  fullname: {
    type: String,
    required: true
  },
  dateofbirth: {
    type: String,
    required: true
  },
  phone: {
    type: String,
    required: true
  },
  job: {
    type: String,
    required: true
  },
  address: {
    type: String
  },
  city: {
    type: String
  },
  region: {
    type: String
  },
  state: {
    type: String
  },
  postalcode: {
    type: Number
  },
  stores: [
    {
      type: Schema.Types.ObjectId,
      ref: 'store'
    }
  ],
  saldo: {
    type: Number,
    default: 0
  },
  userclass: {
    type: String,
    required: true,
    default: 'Normal'
  }
});

module.exports = Profile = mongoose.model('profile', ProfileSchema);
