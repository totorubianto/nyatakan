import mongoose from "mongoose";
import { ObjectID } from "mongodb";

const Schema = mongoose.Schema;

ObjectID.prototype.valueOf = function() {
  return this.toString();
};

const MaterialSchema = new Schema({
  materialname: {
    type: String,
    required: true
  },
  description: {
    type: String
  }
});

export default mongoose.model("Material", MaterialSchema);
