import mongoose from "mongoose";
import { ObjectID } from "mongodb";

const Schema = mongoose.Schema;

ObjectID.prototype.valueOf = function() {
  return this.toString();
};

const ProfileSchema = new Schema({
  fullname: {
    type: String,
    required: true
  },
  dateofbirth: {
    type: String
  },
  phone: {
    type: Number,
    required: true
  },
  job: {
    type: String,
    required: true
  },
  address: {
    type: String,
    required: true
  },
  city: {
    type: String,
    required: true
  },
  region: {
    type: String,
    required: true
  },
  state: {
    type: String,
    required: true
  },
  postalcode: {
    type: Number,
    required: true
  },
  picture: {
    type: String,
    required: true
  },
  saldo: {
    type: Number,
    required: true
  },
  userstatus: {
    type: String,
    required: true
  },
  user: {
    type: Schema.Types.ObjectId,
    ref: "users"
  }
});

export default mongoose.model("Profile", ProfileSchema);
