const mongoose = require("mongoose");

const Schema = mongoose.Schema;

const UserSchema = new Schema({
  profile: {
    type: mongoose.Schema.Types.ObjectId,
    ref: "profile"
  },
  username: {
    type: String,
    required: true,
    unique: true
  },
  email: {
    type: String,
    required: true,
    unique: true
  },
  password: {
    type: String,
    required: true
  },
  userstatus: {
    type: String,
    required: true,
    default:"Active"
  }
});

module.exports = User = mongoose.model('user', UserSchema);