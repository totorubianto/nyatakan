const mongoose = require("mongoose");

const Schema = mongoose.Schema;

const CollectionSchema = new Schema({
  collectionname: {
    type: String,
    required: true
  },
  description: {
    type: String
  },
  start_date: {
    type: Date
  },
  duration: {
    type: Number,
    default:0,
    required: true
  },
  limitofproduct: {
    type: Number
  },
  ishidden: {
    type: Boolean,
    required: true,
    default:false
  },
  products: [
    {
    type: Schema.Types.ObjectId,
    ref: "products"
    }
  ]
});

module.exports = Collection = mongoose.model("collection", CollectionSchema);
