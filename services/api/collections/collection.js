const express = require('express');
const router = express.Router();
const auth = require('../../middleware/auth');
const { check, validationResult } = require('express-validator');

const Store = require('../../models/Store');
const Collection = require('../../models/Collection');
// @route   POST api/collections/:username/create
// @desc    Register user
// @acess   Public
router.post(
  '/:username/create',
  [
    auth(),
    [
      check('collectionname', 'Collection name is required...')
        .not()
        .isEmpty()
    ]
  ],
  async (req, res) => {
    const errors = validationResult(req);
    if (!errors.isEmpty()) {
      return res.status(400).json({ errors: errors.array() });
    }

    const { collectionname, description } = req.body;

    try {
      // see if user exists
      // let user = req.user.id;
      let store = await Store.findOne({ username_store: req.params.username });
      // let collection = await S.findOne({ user: user });      
      console.log(store);
      

      if (!store) {
        return res.status(400).json({
          errors: [{ msg: 'There is no store for this username', param: 'info' }]
        });
      }

      collection = new Collection({
        collectionname, 
        description 
      });
      collection = await collection.save();

      store.collections.unshift(collection.id);

      return res.json(collection);
    } catch (err) {
      res.status(500).send('server error');
    }
  }
);

// @route   DELETE api/collections/:id/delete
// @desc    delete experience from profile
// @acess   Private
router.delete('/:id/delete', auth(), async (req, res) => {
  try {
    const profile = await Profile.findOne({ user: req.user.id });
    const store = await Store.findOne({ collection: req.params.username });

    if (!profile) {
      return res.status(400).json({
        errors: [{ msg: 'There is no profile for this user', param: 'info' }]
      });
    }

    if (!store) {
      return res.status(400).json({
        errors: [{ msg: 'Username of store is not valid...', param: 'info' }]
      });
    }

    await Store.findByIdAndRemove({ _id: store.id });

    // remove index store in profile
    const removeIndex = profile.stores
      .map(item => item.id)
      .indexOf(store.id);
    

    profile.stores.splice(removeIndex, 1);

    await profile.save();

    res.json(profile);
  } catch (err) {
    console.error(err.message);
    res.status(500).send('Server Error');
  }
});

// @route   POST api/profile/update
// @desc    update user
// @acess   Private
router.post('/update', auth(), async (req, res) => {
  const errors = validationResult(req);
  if (!errors.isEmpty()) {
    return res.status(400).json({ errors: errors.array() });
  }

  const { fullname, dateofbirth, phone, job, address, city, region, state, postalcode, saldo, userclass } = req.body;

  const profileFields = {};
  profileFields.id = req.user.id;
  if (fullname) profileFields.fullname = fullname;
  if (dateofbirth) profileFields.dateofbirth = dateofbirth;
  if (phone) profileFields.phone = phone;
  if (job) profileFields.job = job;
  if (address) profileFields.address = address;
  if (city) profileFields.city = city;
  if (region) profileFields.region = region;
  if (state) profileFields.state = state;
  if (postalcode) profileFields.postalcode = postalcode;
  if (saldo) profileFields.saldo = saldo;
  if (userclass) profileFields.userclass = userclass;

  try {
    // Update
    let profile = await Profile.findOneAndUpdate(
      { user: req.user.id },
      { $set: profileFields },
      { new: true }
    ).populate('user', ['email', 'statusclass', 'username']);

    if (!profile) {
      return res.status(400).json({
        errors: [{ msg: 'There is no profile for this user', param: 'info' }]
      });
    }

    return res.json(profile);
  } catch (err) {
    console.error(err.message);
    res.status(500).send('server error');
  }
});

// @route   GET api/profile/all
// @desc    get all profile
// @acess   Public
router.get('/all', async (req, res) => {
  try {
    const profiles = await Profile.find().populate('user', ['email', 'username', 'userstatus']);
    if (!profiles) return res.status(400).json({ msg: 'There is no profile' });
    res.json(profiles);
  } catch (err) {
    console.error(err.message);
    res.status(500).send('Server Error');
  }
});

// @route   GET api/profile/user/:user_id
// @desc    get profile by user id
// @acess   Public
// router.get('/user/:user_id', async (req, res) => {
//     try {
//         const profile = await Profile.findOne({user: req.params.user_id}).populate('user', ['name','avatar']);
//         if(!profile) return res.status(400).json({ msg: 'There is no profile for this user' });
//         res.json(profile);
//     } catch (err) {
//         console.error(err.message);
//         if(err.kind == 'objectId'){
//             return res.status(400).json({ msg: 'Profile is not found' });
//         }
//         res.status(500).send('Server Error');
//     }
// });

// @route   DELETE api/profile/
// @desc    DELETE user,profile, posts
// @acess   Private
// router.delete('/', async (req, res) => {
//     try {
//         // remove profile
//         await Profile.findOneAndRemove({ user: req.params.user_id });
//         // remove user
//         await Profile.findOneAndRemove({ _id: req.params.user_id });

//         res.json({msg : 'User Removed'});
//     } catch (err) {
//         console.error(err.message);
//         if(err.kind == 'objectId'){
//             return res.status(400).json({ msg: 'Profile is not found' });
//         }
//         res.status(500).send('Server Error');
//     }
// });

// @route   PUT api/profile/experience
// @desc    Add profile experience
// @acess   Private
// router.put('/experience', [
//         auth,[
//             check('title', 'Title is required').not().isEmpty(),
//             check('company', 'Company is required').not().isEmpty(),
//             check('from', 'From date is required').not().isEmpty()
//         ]
//     ],
//     async (req, res) => {
//         const errors = validationResult(req);
//         if(!errors.isEmpty()){
//             return res.status(400).json({ errors: errors.array() });
//         }

//         const {
//             title,
//             company,
//             location,
//             from,
//             to,
//             current,
//             description
//         } = req.body;

//         const newExp = {
//             title,
//             company,
//             location,
//             from,
//             to,
//             current,
//             description
//         }

//         try {
//             const profile = await Profile.findOne({ user: req.user.id });

//             profile.experience.unshift(newExp);

//             await profile.save();

//             res.json(profile);
//         } catch (err) {
//             console.error(err.message);
//             res.status(500).send('Server Error');
//         }
//     }
// );

// @route   DELETE api/profile/experience/:exp_id
// @desc    delete experience from profile
// @acess   Private
// router.delete('/experience/:exp_id', auth, async (req, res) =>{
//     try {
//         const profile = await Profile.findOne({ user: req.user.id });

//         // Get remove index
//         const removeIndex = profile.experience.map(item => item.id).indexOf(req.params.exp_id);

//         profile.experience.splice(removeIndex, 1);

//         await profile.save();

//         res.json(profile);
//     } catch (err) {
//         console.error(err.message);
//         res.status(500).send('Server Error');
//     }
// });

// @route   PUT api/profile/education
// @desc    Add profile education
// @acess   Private
// router.put('/education', [
//     auth,[
//         check('school', 'School is required').not().isEmpty(),
//         check('degree', 'Degree is required').not().isEmpty(),
//         check('from', 'From date is required').not().isEmpty()
//     ]
// ],
// async (req, res) => {
//     const errors = validationResult(req);
//     if(!errors.isEmpty()){
//         return res.status(400).json({ errors: errors.array() });
//     }

//     const {
//         school,
//         degree,
//         fieldofstudy,
//         from,
//         to,
//         current,
//         description
//     } = req.body;

//     const newExp = {
//         school,
//         degree,
//         fieldofstudy,
//         from,
//         to,
//         current,
//         description
//     }

//     try {
//         const profile = await Profile.findOne({ user: req.user.id });

//         profile.education.unshift(newExp);

//         await profile.save();

//         res.json(profile);
//     } catch (err) {
//         console.error(err.message);
//         res.status(500).send('Server Error');
//     }
// }
// );

// @route   DELETE api/profile/education/:edu_id
// @desc    delete education from profile
// @acess   Private
// router.delete('/education/:edu_id', auth, async (req, res) =>{
//     try {
//         const profile = await Profile.findOne({ user: req.user.id });

//         // Get remove index
//         const removeIndex = profile.education.map(item => item.id).indexOf(req.params.edu_id);

//         profile.education.splice(removeIndex, 1);

//         await profile.save();

//         res.json(profile);
//     } catch (err) {
//         console.error(err.message);
//         res.status(500).send('Server Error');
//     }
// });

// @route   DELETE api/profile/github/:username
// @desc    Get user repos from Github
// @acess   Public
// router.get('/github/:username', (req, res) =>{
//     try {
//         const option = {
//             uri: `https://api.github.com/users/${
//                 req.params.username
//             }/repos?per_page=5&sort=created:asc&client_id=${
//                 config.get('githubClientId')
//             }&client_secret=${
//                 config.get('githubSecret')
//             }`,
//             method: 'GET',
//             headers: { 'user-agent': 'node.js' }
//         };

//         request(option, (error, response, body) => {
//             if(error) console.log(error);

//             if(response.statusCode !== 200){
//                 res.status(404).json({ msg: 'No Github profile found' });
//             }

//             res.json(JSON.parse(body));
//         });
//     } catch (err) {
//         console.error(err.message);
//         res.status(500).send('Server Error');
//     }
// });

module.exports = router;
