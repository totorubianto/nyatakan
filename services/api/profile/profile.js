const express = require('express');
const router = express.Router();
const auth = require('../../middleware/auth');
const uuidv1 = require('uuid/v1');
const { check, validationResult } = require('express-validator');
const fs = require('fs');
const User = require('../../models/User');
const Profile = require('../../models/Profile');
// @route   POST api/profiles/create
// @desc    Register user
// @acess   Public
router.post(
  '/create',
  [
    auth(),
    [
      check('fullname', 'Full name is required...')
        .not()
        .isEmpty(),
      check('dateofbirth', 'Date of Birth name is required...')
        .not()
        .isEmpty(),
      check('phone', 'Phone is required...')
        .not()
        .isEmpty(),
      check('job', 'Job is required...')
        .not()
        .isEmpty()
    ]
  ],
  async (req, res) => {
    const errors = validationResult(req);
    if (!errors.isEmpty()) {
      return res.status(400).json({ errors: errors.array() });
    }

    const { fullname, dateofbirth, phone, job } = req.body;

    try {
      // see if user exists
      let user = req.user.id;
      let profile = await Profile.findOne({ user: user });

      if (profile) {
        return res
          .status(400)
          .json({ errors: [{ msg: 'Profile already exists', param: 'info' }] });
      }

      profile = new Profile({
        user,
        fullname,
        dateofbirth,
        phone,
        job
      });
      await profile.save();

      user = await User.findById({ _id: user });
      user.profile = profile._id;
      await user.save();

      return res.json(profile);
    } catch (err) {
      res.status(500).send('server error');
    }
  }
);

router.post(
  '/upload',
  [
    auth(),
    [
      check('picture', 'File IMG not Found')
        .not()
        .isEmpty()
    ]
  ],
  async (req, res) => {
    const errors = validationResult(req);
    if (!errors.isEmpty()) {
      return res.status(400).json({ errors: errors.array() });
    }

    const { picture } = req.body;

    try {
      const name = uuidv1() + '.png';
      const path = './public/img/' + name;

      var img = picture;
      let base64Data = '';
      if (img.includes('data:image/jpeg;base64')) {
        base64Data = img.replace(/^data:image\/jpeg;base64,/, '');
      } else {
        base64Data = img.replace(/^data:image\/png;base64,/, '');
      }

      const deleteImg = await Profile.findOne({ user: req.user.id });
      const pathDelete = './public/img/' + deleteImg.picture;

      try {
        fs.unlinkSync(pathDelete);
        require('fs').writeFile(path, base64Data, 'base64', function(err) {
          console.log(err);
        });
        let profileFields = {
          picture: name
        };
        await Profile.findOneAndUpdate(
          { user: req.user.id },
          { $set: profileFields }
        );
        const resProfile = await Profile.findOne({ user: req.user.id });
        res.send(resProfile);
      } catch (err) {
        require('fs').writeFile(path, base64Data, 'base64', function(err) {
          console.log(err);
        });
        let profileFields = {
          picture: name
        };
        await Profile.findOneAndUpdate(
          { user: req.user.id },
          { $set: profileFields }
        );
        const resProfile = await Profile.findOne({ user: req.user.id });

        res.send(resProfile);
      }
    } catch (err) {
      res.status(500).send({
        errors: [{ msg: 'Server Error', param: 'warning' }]
      });
    }
  }
);

// @route   POST api/profile/delete
// @desc    update user
// @acess   Private
router.post('/deletePicture/:idProfile', [auth()], async (req, res) => {
  const errors = validationResult(req);
  if (!errors.isEmpty()) {
    return res.status(400).json({ errors: errors.array() });
  }
  const picture = await Profile.findById(req.params.idProfile);

  try {
    const path = './public/img/' + picture.picture;
    fs.unlinkSync(path);
    let profileFields = {
      picture: ''
    };
    await Profile.findOneAndUpdate(
      { _id: req.params.idProfile },
      { $set: profileFields }
    ).then(profile => res.json(profile));
  } catch (err) {
    res.status(500).send('Server Error');
  }
});

// @route   POST api/profile/update
// @desc    update user
// @acess   Private
// router.post('/update', auth(), async (req, res) => {
//   const errors = validationResult(req);
//   if (!errors.isEmpty()) {
//     return res.status(400).json({ errors: errors.array() });
//   }

//   const {
//     fullname,
//     dateofbirth,
//     phone,
//     job,
//     address,
//     city,
//     region,
//     state,
//     postalcode,
//     saldo,
//     userclass
//   } = req.body;

//   const profileFields = {};
//   profileFields.id = req.user.id;
//   if (fullname) profileFields.fullname = fullname;
//   if (dateofbirth) profileFields.dateofbirth = dateofbirth;
//   if (phone) profileFields.phone = phone;
//   if (job) profileFields.job = job;
//   if (address) profileFields.address = address;
//   if (city) profileFields.city = city;
//   if (region) profileFields.region = region;
//   if (state) profileFields.state = state;
//   if (postalcode) profileFields.postalcode = postalcode;
//   if (saldo) profileFields.saldo = saldo;
//   if (userclass) profileFields.userclass = userclass;

//   try {
//     // Update
//     let profile = await Profile.findOneAndUpdate(
//       { user: req.user.id },
//       { $set: profileFields },
//       { new: true }
//     ).populate('user', ['email', 'statusclass', 'username']);

//     if (!profile) {
//       return res.status(400).json({
//         errors: [{ msg: 'There is no profile for this user', param: 'info' }]
//       });
//     }

//     return res.json(profile);
//   } catch (err) {
//     console.error(err.message);
//     res.status(500).send('server error');
//   }
// });

// @route   GET api/profile/all
// @desc    get all profile
// @acess   Public
router.get('/all', async (req, res) => {
  try {
    const profiles = await Profile.find()
      .populate('user', ['email', 'username', 'userstatus'])
      .populate('stores');
    if (!profiles) return res.status(400).json({ msg: 'There is no profile' });
    res.json(profiles);
  } catch (err) {
    console.error(err.message);
    res.status(500).send('Server Error');
  }
});

// @route   GET api/profile/user/:user_id
// @desc    get profile by user id
// @acess   Public
// router.get('/user/:user_id', async (req, res) => {
//     try {
//         const profile = await Profile.findOne({user: req.params.user_id}).populate('user', ['name','avatar']);
//         if(!profile) return res.status(400).json({ msg: 'There is no profile for this user' });
//         res.json(profile);
//     } catch (err) {
//         console.error(err.message);
//         if(err.kind == 'objectId'){
//             return res.status(400).json({ msg: 'Profile is not found' });
//         }
//         res.status(500).send('Server Error');
//     }
// });

// @route   DELETE api/profile/
// @desc    DELETE user,profile, posts
// @acess   Private
// router.delete('/', async (req, res) => {
//     try {
//         // remove profile
//         await Profile.findOneAndRemove({ user: req.params.user_id });
//         // remove user
//         await Profile.findOneAndRemove({ _id: req.params.user_id });

//         res.json({msg : 'User Removed'});
//     } catch (err) {
//         console.error(err.message);
//         if(err.kind == 'objectId'){
//             return res.status(400).json({ msg: 'Profile is not found' });
//         }
//         res.status(500).send('Server Error');
//     }
// });

// @route   PUT api/profile/experience
// @desc    Add profile experience
// @acess   Private
// router.put('/experience', [
//         auth,[
//             check('title', 'Title is required').not().isEmpty(),
//             check('company', 'Company is required').not().isEmpty(),
//             check('from', 'From date is required').not().isEmpty()
//         ]
//     ],
//     async (req, res) => {
//         const errors = validationResult(req);
//         if(!errors.isEmpty()){
//             return res.status(400).json({ errors: errors.array() });
//         }

//         const {
//             title,
//             company,
//             location,
//             from,
//             to,
//             current,
//             description
//         } = req.body;

//         const newExp = {
//             title,
//             company,
//             location,
//             from,
//             to,
//             current,
//             description
//         }

//         try {
//             const profile = await Profile.findOne({ user: req.user.id });

//             profile.experience.unshift(newExp);

//             await profile.save();

//             res.json(profile);
//         } catch (err) {
//             console.error(err.message);
//             res.status(500).send('Server Error');
//         }
//     }
// );

// @route   DELETE api/profile/experience/:exp_id
// @desc    delete experience from profile
// @acess   Private
// router.delete('/experience/:exp_id', auth, async (req, res) =>{
//     try {
//         const profile = await Profile.findOne({ user: req.user.id });

//         // Get remove index
//         const removeIndex = profile.experience.map(item => item.id).indexOf(req.params.exp_id);

//         profile.experience.splice(removeIndex, 1);

//         await profile.save();

//         res.json(profile);
//     } catch (err) {
//         console.error(err.message);
//         res.status(500).send('Server Error');
//     }
// });

// @route   PUT api/profile/education
// @desc    Add profile education
// @acess   Private
// router.put('/education', [
//     auth,[
//         check('school', 'School is required').not().isEmpty(),
//         check('degree', 'Degree is required').not().isEmpty(),
//         check('from', 'From date is required').not().isEmpty()
//     ]
// ],
// async (req, res) => {
//     const errors = validationResult(req);
//     if(!errors.isEmpty()){
//         return res.status(400).json({ errors: errors.array() });
//     }

//     const {
//         school,
//         degree,
//         fieldofstudy,
//         from,
//         to,
//         current,
//         description
//     } = req.body;

//     const newExp = {
//         school,
//         degree,
//         fieldofstudy,
//         from,
//         to,
//         current,
//         description
//     }

//     try {
//         const profile = await Profile.findOne({ user: req.user.id });

//         profile.education.unshift(newExp);

//         await profile.save();

//         res.json(profile);
//     } catch (err) {
//         console.error(err.message);
//         res.status(500).send('Server Error');
//     }
// }
// );

// @route   DELETE api/profile/education/:edu_id
// @desc    delete education from profile
// @acess   Private
// router.delete('/education/:edu_id', auth, async (req, res) =>{
//     try {
//         const profile = await Profile.findOne({ user: req.user.id });

//         // Get remove index
//         const removeIndex = profile.education.map(item => item.id).indexOf(req.params.edu_id);

//         profile.education.splice(removeIndex, 1);

//         await profile.save();

//         res.json(profile);
//     } catch (err) {
//         console.error(err.message);
//         res.status(500).send('Server Error');
//     }
// });

// @route   DELETE api/profile/github/:username
// @desc    Get user repos from Github
// @acess   Public
// router.get('/github/:username', (req, res) =>{
//     try {
//         const option = {
//             uri: `https://api.github.com/users/${
//                 req.params.username
//             }/repos?per_page=5&sort=created:asc&client_id=${
//                 config.get('githubClientId')
//             }&client_secret=${
//                 config.get('githubSecret')
//             }`,
//             method: 'GET',
//             headers: { 'user-agent': 'node.js' }
//         };

//         request(option, (error, response, body) => {
//             if(error) console.log(error);

//             if(response.statusCode !== 200){
//                 res.status(404).json({ msg: 'No Github profile found' });
//             }

//             res.json(JSON.parse(body));
//         });
//     } catch (err) {
//         console.error(err.message);
//         res.status(500).send('Server Error');
//     }
// });

module.exports = router;

// const express = require('express');
// const router = express.Router();
// const User = require('../../models/User');
// const Profile = require('../../models/Profile');
// const Store = require('../../models/Store');
// const _ = require('lodash');
// // @route   GET api/profile/all
// // @desc    get all profile
// // @acess   Public
// router.get('/all', async (req, res) => {
//   // try {
//   let profile = await Profile.find();
//   let users = await User.find();
//   let store = await Store.find();

//   let hash = Object.create(null);
//   let result = profile.map(
//     (hash => profile =>
//       (hash[profile.user] = {
//         _id: profile._id,
//         key: profile.user,
//         userku: []
//       }))(hash)
//   );

//   users.forEach(
//     (hash => users => {
//       if (_.findKey(profile, ['user', users._id])) {
//         hash[users._id].userku.push({
//           _id: users._id
//         });
//       }
//     })(hash)
//   );

//   let hashStore = Object.create(null);
//   let resultStore = result.map(
//     (hashStore => data =>
//       (hashStore[data._id] = {
//         _id: data._id,
//         key: data.user,
//         userku: data.userku,
//         store: []
//       }))(hashStore)
//   );
//   store.forEach(
//     (hashStore => store => {
//       if (_.findKey(resultStore, ['_id', store.profile])) {
//         hashStore[store.profile].store.push({
//           _id: store._id
//         });
//       } else {
//         console.log('gagal');
//       }
//     })(hashStore)
//   );
//   return res.json(resultStore);
// });

// module.exports = router;
