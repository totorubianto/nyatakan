const express = require('express');
const router = express.Router();
const auth = require('../../middleware/auth');
const nodemailer = require('nodemailer');
const bcrypt = require('bcryptjs');
const jwt = require('jsonwebtoken');
const config = require('config');
const { check, validationResult } = require('express-validator/check');
const connectMail = require('../../config/email');
const User = require('../../models/User');
const Profile = require('../../models/Profile');

// @route   POST api/users/login
// @desc    Authenticate user & get token
// @acess   Public
router.post(
  '/signin',
  [
    check('email', 'Please include a valid username or email...')
      .not()
      .isEmpty(),
    check('password', 'Password is required...')
      .not()
      .isEmpty()
  ],
  async (req, res) => {
    const errors = validationResult(req);
    if (!errors.isEmpty()) {
      return res.status(400).json({ errors: errors.array() });
    }

    const { email, password } = req.body;

    try {
      // see if user exists
      let user = await User.findOne({
        $or: [{ email: email }, { username: email }]
      });

      if (!user) {
        return res.status(400).json({
          errors: [
            { msg: 'Perikasa kembali email atau username', param: 'email' }
          ]
        });
      }

      const isMatch = await bcrypt.compare(password, user.password);
      if (!isMatch) {
        return res.status(400).json({
          errors: [{ msg: 'Kesalahan pada password', param: 'password' }]
        });
      }
      const payload = {
        user: {
          id: user.id
        }
      };

      jwt.sign(
        payload,
        config.get('jwtSecret'),
        { expiresIn: 360000 },
        (err, token) => {
          if (err) throw err;
          res.json({ token });
        }
      );
      // res.send('User registered');
    } catch (err) {
      res.status(400).json({
        errors: [{ msg: 'Kesalahan pada server', param: 'server' }]
      });
    }
  }
);

// @route   POST api/users/signup
// @desc    Register user
// @acess   Public
router.post(
  '/signup',
  [
    check('email', 'Please include a valid email...').isEmail(),
    check(
      'password',
      'Please enter a password with 6 or more characters...'
    ).isLength({ min: 6 }),
    check('username', 'Username is required...').exists()
  ],
  async (req, res) => {
    const errors = validationResult(req);
    if (!errors.isEmpty()) {
      return res.status(400).json({ errors: errors.array() });
    }

    const { email, password, username } = req.body;

    try {
      // see if user exists
      let user = await User.findOne({ email });

      if (user) {
        return res
          .status(400)
          .json({ errors: [{ msg: 'Email already exists', param: 'email' }] });
      }

      user = await User.findOne({ username });
      if (user) {
        return res.status(400).json({
          errors: [{ msg: 'Username already exists', param: 'username' }]
        });
      }

      user = new User({
        password,
        email,
        username
      });
      // encrypt password
      const salt = await bcrypt.genSalt(10);

      user.password = await bcrypt.hash(password, salt);
      await user.save();

      connectMail(2,email,password);

      const payload = {
        user: {
          id: user.id
        }
      };

      jwt.sign(
        payload,
        config.get('jwtSecret'),
        { expiresIn: 360000 },
        (err, token) => {
          if (err) throw err;
          res.json({ token });
        }
      );
      // res.send('User registered');
    } catch (err) {
      res.status(500).send('server error');
    }
  }
);

// @route   POST api/users/signup/api
// @desc    Register user
// @acess   Public
router.post(
  '/signapi',
  [check('email', 'Please include a valid email...').isEmail()],
  async (req, res) => {
    const errors = validationResult(req);
    if (!errors.isEmpty()) {
      return res.status(400).json({ errors: errors.array() });
    }

    const { email } = req.body;

    try {
      // see if user exists
      let user = await User.findOne({
        $or: [{ email: email }, { username: email }]
      });

      if (!user) {
        let username = email;
        let password = Math.random()
          .toString(36)
          .substring(7);

        user = new User({
          password,
          email,
          username
        });
        // encrypt password
        const salt = await bcrypt.genSalt(10);

        user.password = await bcrypt.hash(password, salt);
        await user.save();
        connectMail(email);
      }
      const payload = {
        user: {
          id: user.id
        }
      };

      jwt.sign(
        payload,
        config.get('jwtSecret'),
        { expiresIn: 360000 },
        (err, token) => {
          if (err) throw err;
          res.json({ token });
        }
      );
      // res.send('User registered');
    } catch (err) {
      res.status(500).send('server error');
    }
  }
);

// @route   POST api/users/update
// @desc    update user
// @acess   Private
router.post('/update', auth(), async (req, res) => {
  const errors = validationResult(req);

  if (!errors.isEmpty()) {
    return res.status(400).json({ errors: errors.array() });
  }

  const { 
    username, 
    email, 
    password,
    fullname,
    dateofbirth,
    phone,
    job,
    address,
    city,
    region,
    state,
    postalcode,
    saldo,
    userclass
  } = req.body;

  // Build user object
  const userFields = {};
  userFields.user = req.user.id;

  if (password) userFields.password = password;
  if (username) userFields.username = username;
  if (email) userFields.email = email;

  const profileFields = {};
  profileFields.id = req.user.id;
  if (fullname) profileFields.fullname = fullname;
  if (dateofbirth) profileFields.dateofbirth = dateofbirth;
  if (phone) profileFields.phone = phone;
  if (job) profileFields.job = job;
  if (address) profileFields.address = address;
  if (city) profileFields.city = city;
  if (region) profileFields.region = region;
  if (state) profileFields.state = state;
  if (postalcode) profileFields.postalcode = postalcode;
  if (saldo) profileFields.saldo = saldo;
  if (userclass) profileFields.userclass = userclass;

  try {
    let user = await User.findOne({ email: email });
    if (email != null) {
      if (user) {
        return res
          .status(400)
          .json({ errors: [{ msg: 'Email already exists', param: 'email' }] });
      }
    }

    user = await User.findOne({ username: username });
    if (username != null) {
      if (user) {
        return res.status(400).json({
          errors: [{ msg: 'Username already exists', param: 'username' }]
        });
      }
    }

    user = await User.findOneAndUpdate(
      { _id: req.user.id },
      { $set: userFields },
      { new: true }
    );

    let profile = await Profile.findOneAndUpdate(
      { user: req.user.id },
      { $set: profileFields },
      { new: true }
    ).populate('user', ['email', 'statusclass', 'username']);

    if (!profile) {
      return res.status(400).json({
        errors: [{ msg: 'There is no profile for this user', param: 'info' }]
      });
    }

    return res.json(profile);
  } catch (err) {
    res.status(500).send({
      errors: [{ msg: 'Server Error', param: 'warning' }]
    });
  }
});

// @route   GET api/profile/:username/get
// @desc    get all profile
// @acess   Public
router.get('/:username/get', async (req, res) => {
  try {
    const user = await User.findOne({ username:req.params.username});
    if (!user) return res.status(400).json({ msg: 'There is no user for this username' });
    res.json(user);
  } catch (err) {
    console.error(err.message);
    res.status(500).send('Server Error');
  }
});

// @route   GET api/users/me
// @desc    Get Data From Header
// @acess   Private
router.get('/me', auth(), async (req, res) => {
  try {
    let user = await User.findOne({ _id: req.user.id }).populate({
      path: 'profile',
      model: 'profile',
      populate: {
        path: 'stores',
        model: 'store'
      }
    });
    // console.log(user);

    if (!user) {
      return res.status(400).json({
        errors: [{ msg: 'User not found', param: 'info' }]
      });
    }
    res.json(user);
  } catch (err) {
    res.status(500).send({
      errors: [{ msg: 'Server Error', param: 'warning' }]
    });
  }
});

// @route   GET api/users/forget
// @desc    Get Data From Header
// @acess   Private
router.post('/forget', async (req, res) => {
  const { email } = req.body;
  try {
    let user = await User.findOne({ email: email });

    if (!user) {
      return res.status(400).json({ 
        errors: [
          { msg: 'There is no user for this email ' + email, param: 'info' }
        ]
      });
    }
    let password = Math.random()
      .toString(36)
      .substring(7);
    // encrypt password
    const salt = await bcrypt.genSalt(10);

    user.password = await bcrypt.hash(password, salt);
    await user.save();
    
    connectMail(1,email,password);

    password = { email, password };
    res.json(password);
  } catch (err) {
    res.status(500).send('Server Error');
  }
});
module.exports = router;
