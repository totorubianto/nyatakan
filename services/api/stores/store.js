const express = require('express');
const router = express.Router();
const auth = require('../../middleware/auth');
const gravatar = require('gravatar');
const bcrypt = require('bcryptjs');
const jwt = require('jsonwebtoken');
const config = require('config');
const { check, validationResult } = require('express-validator');

const Store = require('../../models/Store');
const Profile = require('../../models/Profile');

// @route   POST api/stores/create
// @desc    Register user
// @acess   Public
router.post(
  '/create',
  [
    auth(),
    [
      check('store_name', 'Name Store is required...')
        .not()
        .isEmpty(),
      check('username_instagram', 'Instagram Username is required...')
        .not()
        .isEmpty(),
      check('username_youtube', 'Youtube Username is required...')
        .not()
        .isEmpty(),
      check('username_twitter', 'Twitter Username is required...')
        .not()
        .isEmpty(),
      check('username_store', 'Store Username is required...')
        .not()
        .isEmpty()
    ]
  ],
  async (req, res) => {
    const errors = validationResult(req);
    if (!errors.isEmpty()) {
      return res.status(400).json({ errors: errors.array() });
    }

    const { store_name, username_instagram, username_youtube, username_twitter, username_store } = req.body;

    try {
      // Check Auth again
      let profile = await Profile.findOne({ user: req.user.id });
      if (!profile) {
        return res.status(400).json({
          errors: [{ msg: 'There is no profile for this user', param: 'info' }]
        });
      }

      // see if store exists
      let store = await Store.findOne({ username_store: username_store });

      if (store) {
        return res.status(400).json({ errors: [{ msg: 'Store already exists', param: 'info' }] });
      }

      profile = profile._id;
       
      store = new Store({
        profile,
        store_name,
        username_instagram,
        username_youtube,
        username_twitter,
        username_store
      });
      await store.save();

      // Update profile
      // profile.stores.unshift({ _id:store.id});
      // await profile.save();

      return res.json(store);
    } catch (err) {
      console.error(err.message);
      res.status(500).send('server error');
    }
  }
);

// @route   PUT api/stores/:username/update
// @desc    update user
// @acess   Private
router.put('/:username/update', auth, async (req, res) => {
  const errors = validationResult(req);
  if (!errors.isEmpty()) {
    return res.status(400).json({ errors: errors.array() });
  }

  const {
    cover,
    logo,
    store_name,
    username_instagram,
    username_youtube,
    username_twitter,
    username_store,
    ishidden,
    collections
  } = req.body;

  const storeFields = {};
  storeFields.id = req.user.id;
  if (cover) storeFields.cover = cover;
  if (logo) storeFields.logo = logo;
  if (store_name) storeFields.store_name = store_name;
  if (username_instagram) storeFields.username_instagram = username_instagram;
  if (username_youtube) storeFields.username_youtube = username_youtube;
  if (username_twitter) storeFields.username_twitter = username_twitter;
  if (username_store) storeFields.username_store = username_store;
  if (ishidden) storeFields.state = ishidden;
  if (collections) storeFields.postalcode = collections;

  try {
    // Update
    let store = await Store.findOneAndUpdate(
      { username_store: req.params.username },
      { $set: storeFields },
      { new: true }
    );

    if (!store) {
      return res.status(400).json({
        errors: [{ msg: 'Username of store is not valid...', param: 'info' }]
      });
    }

    return res.json(store);
  } catch (err) {
    console.error(err.message);
    res.status(500).send('server error');
  }
});

// @route   GET api/stores/:id/delete
// @desc    delete experience from profile
// @acess   Private
router.get('/:username/get', async (req, res) => {
  try {
    const store = await Store.findOne({ username_store:req.params.username});
    if (!store) return res.status(400).json({ msg: 'There is no store for this username' });
    res.json(store);
  } catch (err) {
    console.error(err.message);
    res.status(500).send('Server Error');
  }
});

// @route   DELETE api/stores/:id/delete
// @desc    delete experience from profile
// @acess   Private
router.delete('/:username/delete', auth(), async (req, res) => {
  try {
    const profile = await Profile.findOne({ user: req.user.id });
    const store = await Store.findOne({ username_store: req.params.username });

    if (!profile) {
      return res.status(400).json({
        errors: [{ msg: 'There is no profile for this user', param: 'info' }]
      });
    }

    if (!store) {
      return res.status(400).json({
        errors: [{ msg: 'Username of store is not valid...', param: 'info' }]
      });
    }

    await Store.findByIdAndRemove({ _id: store.id });

    // remove index store in profile
    const removeIndex = profile.stores.map(item => item.id).indexOf(store.id);

    profile.stores.splice(removeIndex, 1);

    await profile.save();

    res.json(profile);
  } catch (err) {
    console.error(err.message);
    res.status(500).send('Server Error');
  }
});

// @route   GET api/profile/all
// @desc    get all profile
// @acess   Public
router.get('/all', async (req, res) => {
  try {
    const stores = await Store.find().populate('profile');
    if (!stores) return res.status(400).json({ msg: 'There is no stores' });
    return res.json(stores);
  } catch (err) {
    console.error(err.message);
    res.status(500).send('Server Error');
  }
});

// @route   GET api/profile/user/:user_id
// @desc    get profile by user id
// @acess   Public
// router.get('/user/:user_id', async (req, res) => {
//     try {
//         const profile = await Profile.findOne({user: req.params.user_id}).populate('user', ['name','avatar']);
//         if(!profile) return res.status(400).json({ msg: 'There is no profile for this user' });
//         res.json(profile);
//     } catch (err) {
//         console.error(err.message);
//         if(err.kind == 'objectId'){
//             return res.status(400).json({ msg: 'Profile is not found' });
//         }
//         res.status(500).send('Server Error');
//     }
// });

// @route   DELETE api/profile/
// @desc    DELETE user,profile, posts
// @acess   Private
// router.delete('/', async (req, res) => {
//     try {
//         // remove profile
//         await Profile.findOneAndRemove({ user: req.params.user_id });
//         // remove user
//         await Profile.findOneAndRemove({ _id: req.params.user_id });

//         res.json({msg : 'User Removed'});
//     } catch (err) {
//         console.error(err.message);
//         if(err.kind == 'objectId'){
//             return res.status(400).json({ msg: 'Profile is not found' });
//         }
//         res.status(500).send('Server Error');
//     }
// });

// @route   PUT api/profile/experience
// @desc    Add profile experience
// @acess   Private
// router.put('/experience', [
//         auth,[
//             check('title', 'Title is required').not().isEmpty(),
//             check('company', 'Company is required').not().isEmpty(),
//             check('from', 'From date is required').not().isEmpty()
//         ]
//     ],
//     async (req, res) => {
//         const errors = validationResult(req);
//         if(!errors.isEmpty()){
//             return res.status(400).json({ errors: errors.array() });
//         }

//         const {
//             title,
//             company,
//             location,
//             from,
//             to,
//             current,
//             description
//         } = req.body;

//         const newExp = {
//             title,
//             company,
//             location,
//             from,
//             to,
//             current,
//             description
//         }

//         try {
//             const profile = await Profile.findOne({ user: req.user.id });

//             profile.experience.unshift(newExp);

//             await profile.save();

//             res.json(profile);
//         } catch (err) {
//             console.error(err.message);
//             res.status(500).send('Server Error');
//         }
//     }
// );

// @route   DELETE api/profile/experience/:exp_id
// @desc    delete experience from profile
// @acess   Private
// router.delete('/experience/:exp_id', auth, async (req, res) =>{
//     try {
//         const profile = await Profile.findOne({ user: req.user.id });

//         // Get remove index
//         const removeIndex = profile.experience.map(item => item.id).indexOf(req.params.exp_id);

//         profile.experience.splice(removeIndex, 1);

//         await profile.save();

//         res.json(profile);
//     } catch (err) {
//         console.error(err.message);
//         res.status(500).send('Server Error');
//     }
// });

// @route   PUT api/profile/education
// @desc    Add profile education
// @acess   Private
// router.put('/education', [
//     auth,[
//         check('school', 'School is required').not().isEmpty(),
//         check('degree', 'Degree is required').not().isEmpty(),
//         check('from', 'From date is required').not().isEmpty()
//     ]
// ],
// async (req, res) => {
//     const errors = validationResult(req);
//     if(!errors.isEmpty()){
//         return res.status(400).json({ errors: errors.array() });
//     }

//     const {
//         school,
//         degree,
//         fieldofstudy,
//         from,
//         to,
//         current,
//         description
//     } = req.body;

//     const newExp = {
//         school,
//         degree,
//         fieldofstudy,
//         from,
//         to,
//         current,
//         description
//     }

//     try {
//         const profile = await Profile.findOne({ user: req.user.id });

//         profile.education.unshift(newExp);

//         await profile.save();

//         res.json(profile);
//     } catch (err) {
//         console.error(err.message);
//         res.status(500).send('Server Error');
//     }
// }
// );

// @route   DELETE api/profile/education/:edu_id
// @desc    delete education from profile
// @acess   Private
// router.delete('/education/:edu_id', auth, async (req, res) =>{
//     try {
//         const profile = await Profile.findOne({ user: req.user.id });

//         // Get remove index
//         const removeIndex = profile.education.map(item => item.id).indexOf(req.params.edu_id);

//         profile.education.splice(removeIndex, 1);

//         await profile.save();

//         res.json(profile);
//     } catch (err) {
//         console.error(err.message);
//         res.status(500).send('Server Error');
//     }
// });

// @route   DELETE api/profile/github/:username
// @desc    Get user repos from Github
// @acess   Public
// router.get('/github/:username', (req, res) =>{
//     try {
//         const option = {
//             uri: `https://api.github.com/users/${
//                 req.params.username
//             }/repos?per_page=5&sort=created:asc&client_id=${
//                 config.get('githubClientId')
//             }&client_secret=${
//                 config.get('githubSecret')
//             }`,
//             method: 'GET',
//             headers: { 'user-agent': 'node.js' }
//         };

//         request(option, (error, response, body) => {
//             if(error) console.log(error);

//             if(response.statusCode !== 200){
//                 res.status(404).json({ msg: 'No Github profile found' });
//             }

//             res.json(JSON.parse(body));
//         });
//     } catch (err) {
//         console.error(err.message);
//         res.status(500).send('Server Error');
//     }
// });

module.exports = router;
