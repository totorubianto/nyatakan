const express = require('express');
const router = express.Router();
const auth = require('../../../middleware/auth');
const { check, validationResult } = require('express-validator');

const User = require('../../../models/User');
const Profile = require('../../../models/Profile');

// @route   POST api/accounts/update
// @desc    update accounts
// @acess   Private
router.put('/user/:username/update', auth(['Admin']), async (req, res) => {
  const errors = validationResult(req);
  if (!errors.isEmpty()) {
    return res.status(400).json({ errors: errors.array() });
  }

  const { username, email, password, userstatus } = req.body;

  // Build user object
  const userFields = {};
  userFields.user = req.user.id;
  if (password) userFields.password = password;
  if (username) userFields.username = username;
  if (email) userFields.email = email;
  if (userstatus) userFields.userstatus = userstatus;

  try {
    let user = await User.findOne({ email: email });
    if (email != null) {
      if (user) {
        return res
          .status(400)
          .json({ errors: [{ msg: 'Email already exists', param: 'info' }] });
      }
    }

    user = await User.findOne({ username: username });
    if (username != null) {
      if (user) {
        return res.status(400).json({
          errors: [{ msg: 'Username already exists', param: 'info' }]
        });
      }
    }

    user = await User.findOneAndUpdate(
      { username: req.params.username },
      { $set: userFields },
      { new: true }
    );
    return res.json(user);
  } catch (err) {
    console.error(err.message);
    res.status(500).send('server error');
  }
});

// @route   GET api/accounts/all
// @desc    get all accounts
// @acess   Public
router.get('/all/:page', auth(['Admin']), async (req, res) => {
  let perPage = 10;
  let page = req.params.page || 1;

  try {
    const profiles = await Profile
    .find({})
    .skip((perPage * page) - perPage)
    .limit(perPage)
    .populate('user', ['email', 'username', 'userstatus'])
    .populate('stores');

    let count = await Profile.count();

    if (!profiles) return res.status(400).json({ msg: 'There is no profile' });
    res.json({
      profiles: profiles,
      current: page,
      pages: Math.ceil(count / perPage)
    });
  } catch (err) { 
    console.error(err.message);
    res.status(500).send('Server Error');
  }
});

module.exports = router;
