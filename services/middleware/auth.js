const jwt = require('jsonwebtoken');
const config = require('config');

const Profile = require('../models/Profile');

module.exports = function(roles = []) {
  if (typeof roles === 'string') {
    roles = [roles];
  }
  return [
    async (req, res, next) => {
      // Get token from header
      const token = req.header('x-auth-token');
      console.log("Error Head");
      
      // check if not token
      if (!token) {
        return res.status(401).json({
          errors: [{ msg: 'No token, authorization denied', param: 'info' }]
        });
      }

      // Verify token
      try {
        const decode = jwt.verify(token, config.get('jwtSecret'));

        req.user = decode.user;
        if (roles[0]) {
          let profile = await Profile.findOne({ user: req.user.id }).populate('user', [
            'email',
            'username',
            'userstatus'
          ]);
          if (profile) {
            if (roles.length && !roles.includes(profile.userclass)) {
              console.log(roles.length);
              console.log(roles.includes(profile.userclass));
              // user's role is not authorized
              return res.status(401).json({ message: 'Unauthorized' });
            }
          }
        }
        next();
      } catch (err) {
        console.log(err);

        res.status(401).json({ msg: 'Token is not valid ', param: 'info' });
      }
    }
  ];
};
