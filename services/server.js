const express = require('express');
const connectDB = require('./config/db');
const app = express();
const bodyParser = require('body-parser');
const fileUpload = require('express-fileupload');

var cors = require('cors');
connectDB();

app.use(cors());

// Init Middlewere
app.use(fileUpload());

app.use(bodyParser.json({ limit: '10mb' }));
app.use(express.static(__dirname + '/public'));
app.use(bodyParser.urlencoded({ limit: '10mb', extended: true }));

// Define Routes
app.use('/api/users', require('./api/user/user'));
app.use('/api/stores', require('./api/stores/store'));
app.use('/api/profile', require('./api/profile/profile'));
app.use('/api/admin/accounts', require('./api/admin/accounts/accounts'));
app.use('/api/collections', require('./api/collections/collection'));

const PORT = process.env.PORT || 5000;

app.listen(PORT, () => console.log(`Server started on port ${PORT}`));
