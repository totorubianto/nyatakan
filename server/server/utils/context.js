import { PubSub } from 'graphql-yoga';

import { getTokenFromRequest } from './auth';

const pubsub = new PubSub();

export const context = ({ request }) => ({
  pubsub,
  token: getTokenFromRequest(request),
  req: request.request
});
