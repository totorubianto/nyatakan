export const options = {
  port: process.env.PORT || '4006',
  endpoint: '/graphql',
  subscriptions: '/subscriptions',
  playground: '/playground',
  bodyParserOptions: { limit: '10mb', type: 'application/json' },
  cors: {
    credentials: true,
    origin: ['https://localhost:3000'] // App URL.
  }
};
