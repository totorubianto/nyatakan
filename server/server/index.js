require('dotenv').config();
import { GraphQLServer } from 'graphql-yoga';
const cookieParser = require('cookie-parser');
import schema from '../graphql/';
const jwt = require('jsonwebtoken');
import { options } from './utils/options';

const server = new GraphQLServer({
  schema,
  context: req => ({ ...req, token: req.request.headers.authorization })
});

server.express.use(cookieParser());



server.start(options, ({ port }) => {
  console.log(`🚀 Server is running on http://localhost:${port}`);
});
