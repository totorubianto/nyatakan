<h1 align="center">
<br>
  <a href=""><img src="https://i.imgur.com/C4X4AUB.png" alt="GraphQL MongoDB Server" width="128"></a>
<br>
<br>
Nyatakan Server
</h1>

<p align="center">GraphQL, Express and MongoDB. </p>

<hr />

## Introduction

This is a server Nyatakan using GraphQL and MongoDB. Support subscriptions using GraphQL Yoga.

## Getting started

1. Clone this repo using `https://gitlab.com/totorubianto/nyatakan`
2. Move to the appropriate directory: `cd graphql-mongodb-server`.
3. Run `yarn` or `npm install` to install dependencies.
4. Set `.env` file with your mongoURI.
5. Run `npm start` to see the example app at `http://localhost:4000/playground`.
6. database step :
   1. Run `npm install -g mongo-seeding-cli`
   2. Run `seed -u 'mongodb://127.0.0.1:27017/nyatakan' --drop-database ./config/seed`

## Commands

- `npm start` - start the playground at `http://localhost:4000/playground`
