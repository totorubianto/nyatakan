import { mergeResolvers } from "merge-graphql-schemas";

import User from "./Model/User/resolvers";
import Profile from "./Model/Profile/resolvers";
import Store from "./Model/Store/resolvers";
import Collection from "./Model/Collection/resolvers";
import Account from "./Model/Admin/Account/resolvers";

const resolvers = [User,Profile,Store,Collection,Account];

export default mergeResolvers(resolvers);
