import axios from 'axios';

const AccountConnector = {
  getAccounts: async ({ page, token }) => {
    return new Promise((resolve, reject) => {
      if (!token) {
        reject(null);
      } else {
        axios
          .get(`http://localhost:5000/api/admin/accounts/all/${page}`, {
            headers: { 'x-auth-token': token }
          })
          .then(response =>{ 
            return resolve(response.data.profiles)
          })
          .catch(err => reject(err.response.data.errors));
      }
    });
  },
  updateAccount: async ({ username, user, token }) => {
    return new Promise((resolve, reject) => {
      axios
        .put(`http://localhost:5000/api/admin/accounts/user/${username}/update`, user, {
          headers: {
            'x-auth-token': token
          }
        })
        .then(res => {
          resolve(res.data);
        })
        .catch(err => {
          reject(err.response.data);
        });
    });
  }
};

export default AccountConnector;
