import axios from 'axios';
// const { createWriteStream } = require('fs');

const ProfileConnector = {
  getProfile: async ({ _id }) => {
    const error = 'nyatakn';
    return error;
  },
  getProfiles: async () => {
    return await axios
      .get('http://localhost:5000/api/profile/all')
      .then(response => response.data)
      .catch(err => err);
  },
  addProfile: async ({ profile, token }) => {
    return new Promise((resolve, reject) => {
      console.log(token);
      axios
        .post('http://localhost:5000/api/profile/create', profile, {
          headers: { 'x-auth-token': token }
        })
        .then(res => {
          resolve(res.data);
        })
        .catch(err => {
          reject(err.response.data.errors);
        });
    });
  },
  uploadFile: async (picture, { token }) => {
    return new Promise((resolve, reject) => {
      axios
        .post('http://localhost:5000/api/profile/upload', picture, {
          headers: { 'x-auth-token': token }
        })
        .then(res => {
          resolve(res.data);
        })
        .catch(err => {
          reject(err.response.data.errors);
        });
    });
  },
  deletePicture: async (profileID, { token }) => {
    return new Promise((resolve, reject) => {
      const ID = profileID;
      const path = `http://localhost:5000/api/profile/deletePicture/${ID.profileID}`;
      const data = {};
      axios
        .post(
          `http://localhost:5000/api/profile/deletePicture/${ID.profileID}`,
          data,
          {
            headers: { 'x-auth-token': token }
          }
        )
        .then(res => {
          resolve(res.data);
        })
        .catch(err => {
          reject(err.response.data.errors);
        });
    });
  }
  // updateProfile: async ({ profile, token }) => {
  //   return new Promise((resolve, reject) => {
  //     axios
  //       .post('http://localhost:5000/api/profile/update', profile, {
  //         headers: { 'x-auth-token': token }
  //       })
  //       .then(res => {
  //         resolve(res.data);
  //       })
  //       .catch(err => {
  //         reject(err.response.data.errors);
  //       });
  //   });
  // }
};

export default ProfileConnector;
