import axios from 'axios';

const UserConnector = {
  getUser: async ({ username }) => {
    return new Promise((resolve, reject) => {
      axios
        .get(`http://localhost:5000/api/users/${username}/get`)
        .then(response => resolve(response.data))
        .catch(err => reject(err.response.data.errors));
    });
  },
  getAllUsers: async () => {
    return new Promise((resolve, reject) => {
      axios
        .get('http://localhost:5000/api/profile/all')
        .then(response => resolve(response.data))
        .catch(err => reject(err.response.data.errors));
    });
  },
  me: async ({ token }) => {
    return new Promise((resolve, reject) => {
      if (!token) {
        reject([{ msg: 'No Token NO Out..', param: 'info' }]);
      } else {
        axios
          .get('http://localhost:5000/api/users/me', {
            headers: { 'x-auth-token': token }
          })
          .then(response => resolve(response.data))
          .catch(err => reject(err.response.data.errors));
      }
    });
  },
  signUpUser: async ({ user }) => {
    return new Promise((resolve, reject) => {
      axios
        .post('http://localhost:5000/api/users/signup', user)
        .then(res => {
          resolve(res.data.token);
        })
        .catch(err => {
          reject(err.response.data.errors);
        });
    });
  },
  signInUser: async ({ user }) => {
    return new Promise((resolve, reject) => {
      axios
        .post('http://localhost:5000/api/users/signin', user)
        .then(res => {
          resolve(res.data.token);
        })
        .catch(err => {
          reject(err.response.data.errors);
        });
    });
  },
  signApi: async email => {
    return new Promise((resolve, reject) => {
      axios
        .post('http://localhost:5000/api/users/signapi', email)
        .then(res => {
          resolve(res.data.token);
        })
        .catch(err => {
          reject(err.response.data.errors);
        });
    });
  },
 
  updateUser: async ({ user, token }) => {
    return new Promise((resolve, reject) => {
      axios
        .post('http://localhost:5000/api/users/update', user, {
          headers: {
            'x-auth-token': token
          }
        })
        .then(res => {
          resolve(res.data);
        })
        .catch(err => {
          reject(err.response.data.errors);
        });
    });
  },
  forgetPass: async email => {
    return new Promise((resolve, reject) => {
      axios
        .post('http://localhost:5000/api/users/forget', email)
        .then(res => resolve(res.data))
        .catch(err => {
          reject(err.response.data.errors);
        });
    });
  }
};

export default UserConnector;
