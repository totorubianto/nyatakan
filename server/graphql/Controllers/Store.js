import axios from 'axios';

const StoreConnector = {
  getStore: async ({ username }) => {
    return new Promise((resolve, reject) => {
      axios
        .get(`http://localhost:5000/api/stores/${username}/get`)
        .then(response => resolve(response.data))
        .catch(err => reject(err.response.data.errors));
    });
  },
  getStores: async () => {
    return new Promise((resolve, reject) => {
      axios
        .get('http://localhost:5000/api/stores/all')
        .then(response => resolve(response.data))
        .catch(err => reject(err.response.data.errors));
    });
  },

  addStore: async ({ store, token }) => {
    return new Promise((resolve, reject) => {
      axios
        .post('http://localhost:5000/api/stores/create', store, {
          headers: { 'x-auth-token': token }
        })
        .then(res => {
          resolve(res.data);
        })
        .catch(err => reject(err.response.data.errors));
    });
  },
  updateStore: async ({ store, username_store, token }) => {
    return new Promise((resolve, reject) => {
      axios
        .put(`http://localhost:5000/api/stores/${username_store}/update`, store, {
          headers: { 'x-auth-token': token }
        })
        .then(res => {
          resolve(res.data);
        })
        .catch(err => {
          reject(err.response.data.errors);
        });
    });
  },
  deleteStore: async ({ username_store, token }) => {
    return new Promise((resolve, reject) => {
      axios
        .delete(`http://localhost:5000/api/stores/${username_store}/delete`, {
          headers: { 'x-auth-token': token }
        })
        .then(res => {
          resolve(res.data);
        })
        .catch(err => {
          reject(err.response.data.errors);
        });
    });
  }
};

export default StoreConnector;
