import gql from 'graphql-tag';

const typeDefs = gql`
  type User {
    _id: ID
    username: String
    email: String
    userstatus: String
    profile: Profile
    errors: [Error]
  }

  type Forgetpassword {
    email : String
    password : String
    errors : [Error]
  }

  type Query {
    getUser(username: String!): User!
    getAllUsers: [Profile!]!
    isLogin: Boolean!
    me: User
  }

  type Mutation {
    signUpUser(user: signupInput!): Auth
    signInUser(user: signinInput!): Auth
    updateUser(user: updateUserInput): Profile
    forgetPass(email: String): Forgetpassword
    signApi(email: String!): Auth
    logout: Auth
  }

  input updateUserInput {
    email: String
    username: String
    password: String
    fullname: String
    dateofbirth: String
    job: String
    phone: String
    address: String
    city: String
    region: String
    state: String
    postalcode: String
    picture: String
    userclass: String
    saldo: String
  }

  input signupInput {
    email: String
    password: String
    username: String
  }

  input signinInput {
    email: String
    password: String
  }
`;

export default typeDefs;
