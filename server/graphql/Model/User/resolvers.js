import UserConnector from '../../Controllers/User';

const resolvers = {
  Query: {
    getUser: async (parent, { username }, context, info) => {
      let errors = [];
      return UserConnector.getUser({ username })
        .then(user => {
          return {
            user,
            errors
          };
        })
        .catch(err => {
          if (err) {
            err.forEach(function(value) {
              errors.push({ value: value.msg, param: value.param });
            });
            return { token: null, errors };
          }

          throw new Error(err);
        });
    },
    getAllUsers: async (parent, args, context, info) => {
      let errors = [];
      return UserConnector.getAllUsers()
        .then(profile => profile)
        .catch(err => {
          if (err) {
            err.forEach(function(value) {
              errors.push({ value: value.msg, param: value.param });
            });
            return { token: null, errors };
          }

          throw new Error(err);
        });
    },
    me: async (parent, args, { token }) => {
      let errors = [];
      return UserConnector.me({ token })
        .then(me => me)
        .catch(err => {
          if (err) {
            err.forEach(function(value) {
              errors.push({ value: value.msg, param: value.param });
            });
            return { token: null, errors };
          }

          throw new Error(err);
        });
    }
  },
  Mutation: {
    signUpUser: async (parent, { user }, ctx, info) => {
      let errors = [];

      return UserConnector.signUpUser({ user })

        .then(token => {
          // ctx.response.cookie('token', token, {
          //   httpOnly: true,
          //   maxAge: 1000 * 60 * 60 * 24 * 31
          // });
          console.log(token);
          return {
            token,
            errors
          };
        })
        .catch(err => {
          if (err) {
            err.forEach(function(value) {
              errors.push({ value: value.msg, param: value.param });
            });
            return { token: null, errors };
          }

          throw new Error(err);
        });
    },
    signInUser: async (parent, { user }, ctx, info) => {
      const errors = [];

      return UserConnector.signInUser({ user })
        .then(token => {
          // ctx.response.cookie('token', token, {
          //   httpOnly: true,
          //   maxAge: 1000 * 60 * 60 * 24 * 31
          // });
          return {
            token,
            errors
          };
        })
        .catch(err => {
          if (err) {
            err.forEach(function(value) {
              errors.push({ value: value.msg, param: value.param });
            });
            return { token: null, errors };
          }

          throw new Error(err);
        });
    },
    signApi: async (parent, email, ctx, info) => {
      const errors = [];

      return UserConnector.signApi(email)
        .then(token => {
          // ctx.response.cookie('token', token, {
          //   httpOnly: true,
          //   maxAge: 1000 * 60 * 60 * 24 * 31
          // });
          return {
            token,
            errors
          };
        })
        .catch(err => {
          if (err) {
            err.forEach(function(value) {
              errors.push({ value: value.msg, param: value.param });
            });
            return { token: null, errors };
          }

          throw new Error(err);
        });
    },
    updateUser: async (parent, { user }, { token }, info) => {
      const errors = [];

      return UserConnector.updateUser({ user, token })
        .then(user => {
          return {
            user,
            errors
          };
        })
        .catch(err => {
          if (err) {
            err.forEach(function(value) {
              errors.push({ value: value.msg, param: value.param });
            });
            return { token: null, errors };
          }

          throw new Error(err);
        });
    },
    forgetPass: async (parent, email, context, info) => {
      const errors = [];

      return UserConnector.forgetPass(email)
        .then(body => {
          return {
            email: body.email,
            password: body.password,
            errors
          };
        })
        .catch(err => {
          if (err) {
            err.forEach(function(value) {
              errors.push({ value: value.msg, param: value.param });
            });
            return { password: null, errors };
          }

          throw new Error(err);
        });
    },
    logout: async (parent, args, ctx, info) => {
      ctx.response.clearCookie('token');
      return { token: 'berhasil logout' };
    }
  }
};

export default resolvers;
