import gql from "graphql-tag";

const typeDefs = gql`
  type Error {
    value: String
    param: String
  }
`;

export default typeDefs;
