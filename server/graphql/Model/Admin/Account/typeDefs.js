import gql from 'graphql-tag';

const typeDefs = gql`
  type Query {
    getAccounts(page: String): [Profile!]!
  }

  type Mutation {
    updateAccount(username: String ,user: updateAccountInput): User
  }

  input updateAccountInput {
    email: String
    username: String
    password: String
    userstatus: String
  }
`;

export default typeDefs;
