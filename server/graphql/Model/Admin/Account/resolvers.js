import AccountConnector from '../../../Controllers/Account';

const resolvers = {
  Query: {
    getAccounts: async (parent, { page }, { token }) => {
      let errors = [];
      return AccountConnector.getAccounts({ page, token })
        .then(profile => profile)
        .catch(err => {
          if (err) {
            err.forEach(function(value) {
              errors.push({ value: value.msg, param: value.param });
            });
            return { token: null, errors };
          }

          throw new Error(err);
        });
    }
  },
  Mutation: {
    updateAccount: async (parent, { username, user }, { token }, info) => {
      const errors = [];

      return AccountConnector.updateAccount({ username, user, token })
        .then(user => user)
        .catch(err => {
          console.log(err);
          
          if (err) {
            err.forEach(function(value) {
              errors.push({ value: value.msg, param: value.param });
            });
            return { token: null, errors };
          }
          throw new Error(err);
        });
    }
  }
};

export default resolvers;
