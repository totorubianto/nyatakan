import gql from "graphql-tag";

const typeDefs = gql`
  type Collection {
    _id: ID
    cover : String
    logo : String
    collection_name : String 
    username_instagram : String 
    username_youtube : String 
    username_twitter : String
    username_collection : String
    ishidden : String
    collections : [Collection]
    errors: [Error]
  }

  type Query {
    getCollection(_id: ID!): Collection!
    getCollections: [Collection!]!
  }

  type Mutation {
    addCollection(username_collection:String!, collection: collectionInput!): Collection
    updateCollection(collection: collectionInput!): Collection
  }

  input collectionInput {
    cover : String
    logo : String
    collection_name : String 
    username_instagram : String 
    username_youtube : String 
    username_twitter : String
    username_collection : String
    ishidden : String
  }
`;

export default typeDefs;
