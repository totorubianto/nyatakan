import CollectionConnector from '../../Controllers/Collection';

const resolvers = {
  Query: {
    getCollection: async (parent, { _id }, context, info) => {
      return CollectionConnector.getCollection({ _id })
        .then(user => user)
        .catch(err => err);
    },
    getCollections: async (parent, args, context, info) => {
      return CollectionConnector.getCollections()
        .then(collection => collection)
        .catch(err => err);
    }
  },
  Mutation: {
    addCollection: async (parent, { collection }, { token }, info) => {
      const errors = [];

      return CollectionConnector.addCollection(collection, token)
        .then(collection => collection)
        .catch(err => {
          if (err) {
            err.forEach(function(value) {
              errors.push({ param: value.param, value: value.msg });
            });
            return { collection: null, errors };
          }

          throw new Error(err);
        });
    },
    updateCollection: async (parent, { _id, collection }, context, info) => {
      const errors = [];

      return CollectionConnector.updateCollection({ collection, _id })
        .then(collection => collection)
        .catch(err => {
          if (err) {
            err.forEach(function(value) {
              errors.push({ value: value.msg, param: value.param });
            });
            return { collection: null, errors };
          }

          throw new Error(err);
        });
    }
  }
};

export default resolvers;
