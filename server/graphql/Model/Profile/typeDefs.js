import gql from 'graphql-tag';

const typeDefs = gql`
  scalar Upload

  type Profile {
    _id: ID
    fullname: String
    dateofbirth: String
    job: String
    phone: String
    address: String
    city: String
    region: String
    state: String
    postalcode: String
    userclass: String
    picture: String
    saldo: String
    user: User
    stores: [Store]
    errors: [Error]
  }

  type Query {
    getProfile(_id: ID!): Profile!
    getProfiles: [Profile!]!
  }

  type Mutation {
    addProfile(profile: profileInput!): Profile
    uploadFile(picture: String): Profile
    deletePicture(profileID: String): Profile
  }

  input profileInput {
    fullname: String
    dateofbirth: String
    job: String
    phone: String
    address: String
    city: String
    region: String
    state: String
    postalcode: String
    picture: String
    userclass: String
    saldo: String
  }
`;

export default typeDefs;
