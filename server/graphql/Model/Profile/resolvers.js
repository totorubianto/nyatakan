import ProfileConnector from '../../Controllers/Profile';

const resolvers = {
  Query: {
    getProfile: async (parent, { _id }, context, info) => {
      return ProfileConnector.getProfile({ _id })
        .then(user => user)
        .catch(err => err);
    },
    getProfiles: async (parent, args, context, info) => {
      return ProfileConnector.getProfiles()
        .then(profile => profile)
        .catch(err => err);
    }
  },
  Mutation: {
    addProfile: async (parent, { profile }, { token }, info) => {
      const errors = [];

      return ProfileConnector.addProfile({ profile, token })
        .then(profile => profile)
        .catch(err => {
          if (err) {
            err.forEach(function(value) {
              errors.push({ param: value.param, value: value.msg });
            });
            return { profile: null, errors };
          }

          throw new Error(err);
        });
    },
    uploadFile: async (parent, picture, { token }, info) => {
      const errors = [];

      return ProfileConnector.uploadFile(picture, { token })
        .then(result => result)
        .catch(err => {
          if (err) {
            err.forEach(function(value) {
              errors.push({ param: value.param, value: value.msg });
            });
            return { profile: null, errors };
          }
          throw new Error(err);
        });
    },
    deletePicture: async (parent, pictureID, { token }, info) => {
      const errors = [];

      return ProfileConnector.deletePicture(pictureID, { token })
        .then(result => result)
        .catch(err => {
          if (err) {
            err.forEach(function(value) {
              errors.push({ param: value.param, value: value.msg });
            });
            return { profile: null, errors };
          }
          throw new Error(err);
        });
    }
  }
};

export default resolvers;
