import StoreConnector from '../../Controllers/Store';

const resolvers = {
  Query: {
    getStore: async (parent, { username }, context, info) => {
      let errors = [];
      return StoreConnector.getStore({ username })
        .then(store => {
          return {
            store,
            errors
          };
        })
        .catch(err => {
          console.log(err);

          if (err) {
            err.forEach(function(value) {
              errors.push({ value: value.msg, param: value.param });
            });
            return { token: null, errors };
          }

          throw new Error(err);
        });
    },
    getStores: async (parent, args, context, info) => {
      const errors = [];
      return StoreConnector.getStores()
        .then(store => {
          store.errors = errors;
          return store;
        })
        .catch(err => err);
    }
  },
  Mutation: {
    addStore: async (parent, { store }, { token }, ctx, info) => {
      const errors = [];

      return StoreConnector.addStore({ store, token })
        .then(store => {
          store.errors = errors;
          return {
            store,
            errors
          };
        })
        .catch(err => {
          if (err) {
            err.forEach(function(value) {
              errors.push({ param: value.param, value: value.msg });
            });
            return { store: null, errors };
          }

          throw new Error(err);
        });
    },
    updateStore: async (parent, { username_store, store }, { token }, info) => {
      const errors = [];

      return StoreConnector.updateStore({ store, username_store, token })
        .then(store => {
          return {
            store,
            errors
          };
        })
        .catch(err => {
          if (err) {
            err.forEach(function(value) {
              errors.push({ value: value.msg, param: value.param });
            });
            return { store: null, errors };
          }

          throw new Error(err);
        });
    },
    deleteStore: async (parent, { username_store }, { token }, info) => {
      const errors = [];

      return StoreConnector.deleteStore({ username_store, token })
        .then(store => {
          return {
            store,
            errors
          };
        })
        .catch(err => {
          if (err) {
            err.forEach(function(value) {
              errors.push({ value: value.msg, param: value.param });
            });
            return { store: null, errors };
          }

          throw new Error(err);
        });
    }
  }
};

export default resolvers;
