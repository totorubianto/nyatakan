import gql from "graphql-tag";

const typeDefs = gql`
  type Store {
    _id: ID
    cover : String
    logo : String
    profile : Profile
    store_name : String 
    username_instagram : String 
    username_youtube : String 
    username_twitter : String
    username_store : String
    ishidden : String
    collections : [Collection]
    errors: [Error]
  }

  type Query {
    getStore(username: String!): Store!
    getStores: [Store!]!
  }

  type Mutation {
    addStore(store: storeInput!): Store
    updateStore(username_store:String!, store: storeInput!): Store
    deleteStore(username_store:String!): Store
  }

  input storeInput {
    cover : String
    logo : String
    store_name : String 
    username_instagram : String 
    username_youtube : String 
    username_twitter : String
    username_store : String
    ishidden : String
  }
`;

export default typeDefs;
