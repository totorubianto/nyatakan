import { mergeTypes } from "merge-graphql-schemas";

import User from "./Model/User/typeDefs";
import Profile from "./Model/Profile/typeDefs";
import Collection from "./Model/Collection/typeDefs";
import Store from "./Model/Store/typeDefs";
import Account from "./Model/Admin/Account/typeDefs";
import Auth from "./Model/Auth/typeDefs";
import Error from "./Model/Error/typeDefs";

const typeDefs = [User, Auth, Profile, Store, Collection, Account, Error];

export default mergeTypes(typeDefs);
